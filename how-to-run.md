# How to run?
This document shows how to run the code.

### 1. Get the repository

`>> git clone https://bitbucket.org/zStrzinar/sop.git`

### 2. build docker images

```
>> cd sop/docker
>> docker build sop-base -t sop-base:latest --no-cache
>> docker build device -t device:latest
>> docker build ingress -t ingress:latest
>> docker build preparation -t preparation:latest
>> docker build analysis -t analysis:latest
>> docker build kafka-consumer -t kafka-consumer:latest
```

### 3. modify `docker-compose.yml`
In docker-compose file you may wish to modify:

* `KAFKA_ADVERTISED_HOST_NAME`: set this to your IP

* `INGRESS_REDIS_NAMESPACE`: you may leave this as is or you can manually determine a redis namespace

* `ANALYSIS_ARCHIVE_CONSUMER_GROUP`, `ANALYSIS_EVENTS_CONSUMER_GROUP`: you may leave this as is or you can set your own 
consumer groups

* `ES_ARCHIVE_INDEX`, `ES_MODEL_INDEX`, `ES_EVENT_INDEX`: ElasticSearch index settings


### 4. run

`>> docker-compose up`

### 5. inspect the results

Open your browser and go to http://localost:5601 to open [Kibana](https://www.elastic.co/kibana) - Elasticsearch web UI.
If this is your first time using Kibana you will be greeted by a welcome screen. Because we are already saving our own
data to elasticsearch, click `Explore on my own`.

Click in the upper left corner and under the 'Kibana' tab click 'Discover'. You should be prompted to create an index 
pattern. Index patterns are Kibana's way of accessing Elasticsearch data. In the Index pattern write `archive*` and go 
to the next step. There, chose `timestamp` as our Time Filter field name. Then Create the index patter.

Again go to Discover and you should see some data in your database. Because you are looking at the `archive*` index 
pattern this is raw data.

In `Management/Stack Management/Kibana/Index Patterns` (upper left menu) you should add additional index patterns: 
`events*` and `models*`.

Then, when you have added additional index patterns, again go to Discover and chose a different index pattern (arrow 
next to `archive*`). In the `events*` you should see events, in `models*` ou should see a model created. If your docker 
compose has been running for more than 15 minutes you may need to change the timeframe from `Last 15 minutes`.

### 6. congratulations! 
