from json import loads
from uuid import uuid4
import argparse, os, logging

from kafka import TopicPartition
from kafka.consumer import KafkaConsumer

from crosscutting.constants.kafka import KAFKA_BROKER


def run_consumer(topic: str, broker: str, consumer_group: str = None, max_messages: int = 100):
    consumer = KafkaConsumer(topic, bootstrap_servers=[broker], auto_offset_reset='latest', enable_auto_commit=True,
                             group_id=consumer_group if consumer_group is not None else str(uuid4()),
                             value_deserializer=lambda x: loads(x.decode('utf-8')))
    consumer.poll(timeout_ms=1,max_records=1)

    messages = 0
    logger = logging.getLogger('sop-logger')

    while (messages < max_messages) if max_messages > 0 else True:
        result = consumer.poll(timeout_ms=100, max_records=1)
        if result == {}:
            pass
        else:
            logger.debug(result[TopicPartition(topic, 0)][0].value)
            messages = messages+1


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Kafka console consumer.")
    parser.add_argument('-k', "--kafka", help="Kafka broker host:port. KAFKA_BROKER environment variable.",
                        default=os.environ.get('KAFKA_BROKER', KAFKA_BROKER))
    parser.add_argument('-t', '--topic', help="Kafka topic to listen. KAFKA_TOPIC environment variable.",
                        default=os.environ.get('KAFKA_TOPIC', 'test'))
    parser.add_argument('-g', '--consumer_group', default=str(uuid4()))
    args = parser.parse_args()

    logFormatter = logging.Formatter("%(asctime)s %(levelname)s @ %(funcName)s: %(message)s")
    rootLogger = logging.getLogger("sop-logger")
    rootLogger.setLevel(logging.DEBUG)

    fileHandler = logging.FileHandler("log.log")
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)

    run_consumer(topic=args.topic, broker=args.kafka, consumer_group=args.consumer_group, max_messages=0)
