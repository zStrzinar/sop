import pytest
import queue
import threading
import time
from uuid import UUID, uuid4
import datetime
from typing import Tuple
import json

from preparation.consumer import PreparationConsumer
from crosscutting.constants.kafka import KAFKA_BROKER
from tests.kafka_producer import send_messages
from preparation.processor import PreparationProcessor, Segmentor, Batch, Segment, List
from preparation.producer import PreparationProducer


from kafka.errors import NoBrokersAvailable


# region Preparation Layer Kafka Consumer
def test_preparation_consumer_init():
    preparation_consumer = PreparationConsumer(kafka_broker=KAFKA_BROKER,
                                               kafka_topic='test_topic',
                                               kafka_consumer_group=str(uuid4()))

    with pytest.raises(NoBrokersAvailable):
        PreparationConsumer(kafka_broker=f"{KAFKA_BROKER}9",
                            kafka_topic='test_topic',
                            kafka_consumer_group=str(uuid4()))
    pass


def test_preparation_consumer_run():
    topic: str = str(uuid4())
    preparation_consumer = PreparationConsumer(kafka_broker=KAFKA_BROKER,
                                               kafka_topic=topic,
                                               kafka_consumer_group=str(uuid4()))
    preparation_consumer.convert_dict_to_batch = False
    n_messages = 50
    send_messages(broker=KAFKA_BROKER, topic=topic, sleep=None,
                  messages=[{"key1": "value1", "key2": i} for i in range(n_messages)])
    q = queue.Queue()
    e = threading.Event()
    t = threading.Thread(target=preparation_consumer.run, args=(q, e), daemon=True)
    t.start()
    start = datetime.datetime.now()
    while (q.qsize() < n_messages) and datetime.datetime.now() < start + datetime.timedelta(seconds=10):
        pass
    e.set()
    assert q.qsize() == n_messages
# endregion


# region Preparation Layer Processor
def test_preparation_processor_run():
    in_queue = queue.Queue(maxsize=100)
    out_queue = queue.Queue(maxsize=10000)

    class TestSegmentor(Segmentor):
        def segment(self, batch: Batch) -> List[Segment]:
            batch_len: int = len(batch.values) if type(batch.values) is list else len(batch.values.values)
            segment_len: int = self.default_length
            segments: List[Segment] = []
            total_length: int = 0
            while total_length < batch_len:
                segments.append(Segment.generate_random(timestamp=batch.timestamp,
                                                        source_device_uuid=batch.source_device_uuid,
                                                        location=batch.location,
                                                        production_line=batch.production_line,
                                                        machine=batch.machine,
                                                        n=segment_len))
                total_length = total_length + segment_len
            return segments

    processor = PreparationProcessor(TestSegmentor())

    """ run and stop """
    stop_event: threading.Event = threading.Event()
    run_thread: threading.Thread = threading.Thread(target=processor.run, args=(in_queue, out_queue, stop_event),
                                                    daemon=True)
    run_thread.start()
    time.sleep(1)
    stop_event.set()
    run_thread.join(timeout=1)
    assert not run_thread.is_alive()

    """ run, see data in output queue """
    source_uuid: UUID = uuid4()
    multiplier: int = 10
    in_queue.put(Batch.generate_random(source_device_uuid=source_uuid, n=TestSegmentor.default_length*multiplier))
    stop_event.clear()
    run_thread: threading.Thread = threading.Thread(target=processor.run, args=(in_queue, out_queue, stop_event),
                                                    daemon=True)
    run_thread.start()
    time.sleep(1)
    stop_event.set()
    run_thread.join(timeout=1)
    assert not run_thread.is_alive()
    assert out_queue.qsize() == multiplier
# endregion


# region Preparation Layer Producer
def test_preparation_procuer_init() -> Tuple[PreparationProducer, str]:
    test_topic = "segments_testing_topic_vtzzvuz"
    preparation_producer = PreparationProducer(kafka_broker=KAFKA_BROKER, kafka_topic=test_topic)
    assert preparation_producer.kafka_producer.bootstrap_connected()
    return preparation_producer, test_topic


def test_preparation_producer_send():
    from kafka import KafkaConsumer, TopicPartition
    segment = Segment.generate_random()
    preparation_producer, test_topic = test_preparation_procuer_init()
    preparation_producer: PreparationProducer
    preparation_producer._send(segment)
    assert True
    consumer = KafkaConsumer(test_topic, bootstrap_servers=KAFKA_BROKER, auto_offset_reset='latest',
                             enable_auto_commit=True, group_id=str(uuid4()),
                             value_deserializer=lambda x: json.loads(x.decode('utf-8')))
    assert consumer.bootstrap_connected()
    message = consumer.poll(timeout_ms=10)
    assert message == {}
    preparation_producer._send(segment)
    message = consumer.poll(timeout_ms=10)
    assert message != {}
    assert message[TopicPartition(test_topic, 0)][0].value == segment.__dict__()


def test_preparation_producer_run():
    from kafka import KafkaConsumer, TopicPartition

    segment = Segment.generate_random()
    preparation_producer, test_topic = test_preparation_procuer_init()
    preparation_producer: PreparationProducer

    in_queue: queue.Queue = queue.Queue(maxsize=100)
    stop_event: threading.Event = threading.Event()

    """ run and stop """
    producer = threading.Thread(target=preparation_producer.run, args=(in_queue, stop_event), daemon=True)
    producer.start()
    time.sleep(1)
    stop_event.set()
    producer.join()
    assert not producer.is_alive()
    stop_event.clear()

    """ run and see messages in kafka """

    producer = threading.Thread(target=preparation_producer.run, args=(in_queue, stop_event), daemon=True)
    producer.start()
    consumer = KafkaConsumer(test_topic, bootstrap_servers=KAFKA_BROKER, auto_offset_reset='latest',
                             enable_auto_commit=True, group_id=str(uuid4()),
                             value_deserializer=lambda x: json.loads(x.decode('utf-8')))
    assert consumer.bootstrap_connected()
    message = consumer.poll(timeout_ms=10, max_records=100)
    assert message == {}
    segments: List[Segment] = [Segment.generate_random()]#, Segment.generate_random(), Segment.generate_random()]
    assert in_queue.qsize() == 0
    messages: List[dict] = []
    for s in segments:
        in_queue.put(s)

    while True:
        message = consumer.poll(timeout_ms=100, max_records=1)
        if message == {}:
            break
            #raise Exception("Didn't received sent message")
        messages.append(message[TopicPartition(test_topic, 0)][0].value)

    assert len(messages) == len(segments)
    for i in range(len(messages)):
        assert messages[i] == segments[i].__dict__()
    stop_event.set()
# endregion
