import pytest
import time
from uuid import uuid4
from threading import Event

from analysis import ModelFacade, ModelDbEntry
from analysis.example.model import ExampleModel as Model
from analysis.db import DB, ES

from crosscutting.constants.db import ES_HOST, ES_MODEL_INDEX


def test_create_facade():
    es = ES(host=ES_HOST, default_index=ES_MODEL_INDEX)
    es.rebuild()
    refresh_event = Event()

    facade = ModelFacade(db=es, model_identifier=uuid4(), db_key="model", refresh_event=refresh_event)


def test_create_facade_with_model():
    es = ES(host=ES_HOST, default_index=ES_MODEL_INDEX)
    es.rebuild()
    refresh_event = Event()

    model: Model = Model(learning_set=[])

    facade = ModelFacade.from_model(db=es, model_identifier=uuid4(), db_key="model", refresh_event=refresh_event,
                                    model=model)

    return facade, model


def test_create_facade_from_db():
    es = ES(host=ES_HOST, default_index=ES_MODEL_INDEX)
    refresh_event = Event()

    f, m = test_create_facade_with_model()
    time.sleep(1)
    facade = ModelFacade.from_db(db=es, model_identifier=f.model_id, db_key="model", refresh_event=refresh_event,
                                 model_type=Model)
    model = facade.model

    assert facade.__str__() == f.__str__()
    assert model.__str__() == m.__str__()
