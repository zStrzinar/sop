import requests, datetime, time
from uuid import uuid4, UUID
from requests import Response

from crosscutting.constants.ingress import INGRESS_API_DEFAULT_HOST, INGRESS_API_DEFAULT_PORT, INGRESS_WATCHDOG_INTERVAL

url_base = f"http://{INGRESS_API_DEFAULT_HOST}:{INGRESS_API_DEFAULT_PORT}"


def test_batch_ingress_api():
    device_uuid: UUID = uuid4()
    data: dict = {
        "device_uuid": str(device_uuid)
    }
    response: Response = requests.post(url=f"{url_base}/registration", json=data, timeout=10)
    assert response.status_code == 202  # HTTP 202 = Accepted

    data: dict = {
        "timestamp": "2020-07-28T15:19:00.000+02:00",
        "source_device_uuid": str(device_uuid),
        "values": {
            "sample_frequency_hz": 100,
            "values": [10, 11, 12, 13, 14, 15]
        }
    }
    response: Response = requests.post(url=f"{url_base}/batch", json=data, timeout=10)
    assert response.status_code == 201  # HTTP 201 = created


def test_device_registration_api():
    device_uuid = uuid4()
    data: dict = {
        "device_uuid": str(device_uuid)
    }
    response: Response = requests.post(url=f"{url_base}/registration", json=data, timeout=10)
    assert response.status_code == 202  # HTTP 202 = Accepted

    response: Response = requests.post(url=f"{url_base}/registration", json=data, timeout=10)
    assert response.status_code == 409  # HTTP 409 = Conflict


def test_watchdog_api():
    device_uuid = uuid4()
    data: dict = {
        "device_uuid": str(device_uuid)
    }
    response: Response = requests.post(url=f"{url_base}/registration", json=data, timeout=10)
    assert response.status_code == 202  # HTTP 202 = Accepted

    watchdog_interval: datetime.timedelta = INGRESS_WATCHDOG_INTERVAL
    response: Response = requests.post(url=f"{url_base}/watchdog", json=data, timeout=10)
    assert response.status_code == 200  # HTTP 200 = OK
    time.sleep(0.5)
    response: Response = requests.post(url=f"{url_base}/watchdog", json=data, timeout=10)
    assert response.status_code == 200  # HTTP 200 = OK
    response: Response = requests.post(url=f"{url_base}/watchdog", json={"device_uuid": str(uuid4())},
                                       timeout=10)
    assert response.status_code == 404  # HTTP 404 = Not found

    time.sleep((watchdog_interval+datetime.timedelta(seconds=1)).total_seconds())
    response: Response = requests.post(url=f"{url_base}/watchdog", json=data, timeout=10)
    assert response.status_code == 410  # HTTP 410 = Gone


def test_device_registration_api_delete():
    device_uuid = uuid4()
    data: dict = {
        "device_uuid": str(device_uuid)
    }
    response: Response = requests.post(url=f"{url_base}/registration", json=data, timeout=10)
    assert response.status_code == 202  # HTTP 202 = Accepted

    response: Response = requests.delete(url=f"{url_base}/registration/{device_uuid}", timeout=10)
    assert response.status_code == 200  # HTTP 200 = OK


def test_device_registration_api_put():
    device_uuid = uuid4()

    response: Response = requests.put(url=f"{url_base}/registration/{device_uuid}", timeout=10)
    assert response.status_code == 202  # HTTP 202 = Accepted

    response: Response = requests.put(url=f"{url_base}/registration/{device_uuid}", timeout=10)
    assert response.status_code == 409  # HTTP 409 = Conflict
