import random, datetime
from json import loads, dumps
from uuid import uuid4

from kafka import TopicPartition
from kafka.consumer import KafkaConsumer
from kafka.producer import KafkaProducer

from crosscutting.constants.kafka import KAFKA_BROKER


def test_kafka_produce_consume(broker: str = KAFKA_BROKER):
    topic: str = str(uuid4())
    consumer = KafkaConsumer(topic, bootstrap_servers=[broker], auto_offset_reset='latest',
                             value_deserializer=lambda x: loads(x.decode('utf-8')), enable_auto_commit=True, group_id=str(uuid4()))
    consumer.poll(timeout_ms=1, max_records=1)
    producer = KafkaProducer(bootstrap_servers=[broker],
                             value_serializer=lambda x: dumps(x).encode('utf-8'))
    data = {"data": random.random()}
    future = producer.send(topic, value=data)
    result = future.get(timeout=10)
    producer.flush()
    start = datetime.datetime.utcnow()
    while datetime.datetime.utcnow() < (start + datetime.timedelta(seconds=10)):
        result = consumer.poll(timeout_ms=100, max_records=1)
        if result == {}:
            pass
        else:
            print(result)
            break
    assert result[TopicPartition(topic, 0)][0].value == data