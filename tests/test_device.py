import pytest
from device.device import Device, DeviceIngressChannelType
from crosscutting.entities.rawbatch import RawBatch
from crosscutting.constants.ingress import INGRESS_API_DEFAULT_HOST, INGRESS_API_DEFAULT_PORT


# region API communication to ingress layer
def test_device_api_construction():
    from uuid import uuid4, UUID

    def test_api_device(device_: Device):
        assert device_.ingress_channel_type == DeviceIngressChannelType.API
        assert type(device_.ingress_channel_config) is Device._APIChannelConfig
        assert type(device_.device_uuid) is UUID
        pass

    def test_default_api_channel_config(config: Device._APIChannelConfig):
        assert config.CHANNEL_TYPE == DeviceIngressChannelType.API
        assert config.base_url.find("http") != -1
        assert config.base_url.find(INGRESS_API_DEFAULT_HOST) != -1
        assert config.base_url.find(str(INGRESS_API_DEFAULT_PORT)) != -1
        pass

    """ __init__ constructor """
    device = Device(uuid=uuid4())
    test_api_device(device)
    test_default_api_channel_config(device.ingress_channel_config)
    device_2 = Device(uuid=uuid4())
    assert device.device_uuid != device_2.device_uuid

    """ get_new_api_device() constructor """
    device = Device.get_new_api_device()
    test_api_device(device)
    test_default_api_channel_config(device.ingress_channel_config)
    device_2 = Device.get_new_api_device()
    assert device.device_uuid != device_2.device_uuid
    pass


def test_device_api_try_send_batch():
    device = Device.get_new_api_device(register=True)

    """ timestamped batch """
    batch: RawBatch = RawBatch.generate_timestamped(n=10, source_device_uuid=device.device_uuid)
    result = device._try_send_batch_API(batch=batch)
    assert result is True

    """ sampled batch """
    batch: RawBatch = RawBatch.generate_sampled(n=10, source_device_uuid=device.device_uuid)
    result = device._try_send_batch_API(batch=batch)
    assert result is True


def test_device_api_try_register():
    device = Device.get_new_api_device()

    """ register """
    result = device._try_register_API()
    assert result is True

    """ unsucessfully re-register """
    result = device._try_register_API()
    assert result is False


def test_device_api_try_unregister():
    device = Device.get_new_api_device()

    """ register """
    result = device._try_register_API()
    assert result is True

    """ unregister """
    result = device._try_unregister_API()
    assert result is True

    """ repeat unregister """
    result = device._try_unregister_API()
    assert result is True

    """ re-register """
    result = device._try_register_API()
    assert result is True
# endregion
