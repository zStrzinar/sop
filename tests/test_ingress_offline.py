import pytest, datetime
from uuid import UUID, uuid4
import queue
from ingress.app import IngressApp
from crosscutting.entities.rawbatch import RawBatch

def test_batch_ingress_function():
    device_uuid: UUID = uuid4()
    body: dict = {
        "timestamp": "2020-07-28T15:19:00.000+02:00",
        "source_device_uuid": str(device_uuid),
        "values": {
            "sample_frequency_hz": 100,
            "values": [10, 11, 12, 13, 14, 15]
        }
    }
    q = queue.Queue(maxsize=50)
    app = IngressApp()
    app.device_registration({"device_uuid": str(device_uuid)}, request_id=uuid4())
    app.raw_batches_queue = q
    message, status_code = app.batch_ingress(body, request_id=uuid4())
    assert status_code == 201  # HTTP 201 = Created
    assert q.qsize() == 1


def test_device_registration_function():
    app = IngressApp()
    device_uuid: UUID = uuid4()
    body: dict = {
        "device_uuid": str(device_uuid)
    }

    message, status_code = app.device_registration(data=body, request_id=uuid4())
    assert status_code == 202  # HTTP 202 = Accepted
    message, status_code = app.device_registration(data=body, request_id=uuid4())
    assert status_code == 409  # HTTP 409 = Conflict
    assert len(app.registered_devices) == 1
    assert app.registered_devices[0].device_uuid == device_uuid


def test_is_device_registered():
    app = IngressApp()
    body, device_uuid = generate_and_register_device(app)
    registration_time: datetime.datetime = app.registered_devices[0].registration_timestamp
    watchdog_interval: datetime.timedelta = app.registered_devices[0].watchdog_interval

    assert app.is_device_registered(device_uuid=device_uuid, time=registration_time + watchdog_interval / 10)
    assert app.is_device_registered(device_uuid=device_uuid, time=registration_time + watchdog_interval * 1.1) == -1
    assert app.is_device_registered(device_uuid=device_uuid, time=registration_time - watchdog_interval / 10) == -1

    app.registered_devices = []
    assert app.is_device_registered(device_uuid=device_uuid, time=registration_time + watchdog_interval / 10) == 0

    assert len(app.registered_devices) == 0
    message, status_code = app.device_registration(data=body, request_id=uuid4())
    assert status_code == 202  # HTTP 202 = Accepted
    assert len(app.registered_devices) == 1
    now = datetime.datetime.utcnow()
    app.registered_devices.append(IngressApp.DeviceRegistration(device_uuid=device_uuid,
                                                                registration_timestamp=now,
                                                                last_watchdog=now,
                                                                watchdog_interval=datetime.timedelta(seconds=20)))
    with pytest.raises(Exception):
        app.is_device_registered(device_uuid=device_uuid)
    pass


def generate_and_register_device(app: IngressApp):
    """ Generate device_uuid, register device, check response and resulting registrations list

    :return: registration data body, device_uuid
    :raises AssertionError: id status code is not 202 (Accepted), start od end list lengthts are not 0 and 1
    """
    if len(app.registered_devices) > 0:
        app.reinit()
    assert len(app.registered_devices) == 0
    device_uuid: UUID = uuid4()
    body: dict = {
        "device_uuid": str(device_uuid)
    }
    message, status_code = app.device_registration(data=body, request_id=uuid4())
    assert status_code == 202  # HTTP 202 = Accepted
    assert len(app.registered_devices) == 1
    return body, device_uuid


def test_watchdog_function():
    app = IngressApp()
    body, device_uuid = generate_and_register_device(app)
    registration_timestamp: datetime.datetime = app.registered_devices[0].registration_timestamp
    assert app.registered_devices[0].last_watchdog == registration_timestamp
    watchdog_interval: datetime.timedelta = app.registered_devices[0].watchdog_interval
    new_watchdog_time: datetime.datetime = registration_timestamp + watchdog_interval / 2
    body = {"device_uuid": str(device_uuid)}
    assert app.register_device_watchdog(data=body, time=new_watchdog_time) == 1  # OK
    assert app.registered_devices[0].last_watchdog == new_watchdog_time

    new_watchdog_time = new_watchdog_time + watchdog_interval * 1.1
    assert app.register_device_watchdog(data=body,
                                        time=new_watchdog_time) == -1  # Watchdog violated

    assert app.register_device_watchdog(data={"device_uuid": str(uuid4())},
                                        time=registration_timestamp) == 0  # Not registered

    assert app.register_device_watchdog(data={"incorrect_key": "odd_value"},
                                        time=registration_timestamp) == -2  # Incorrect dictionary passed
    pass


def test_remove_registration():
    app = IngressApp()
    body, device_uuid = generate_and_register_device(app)

    result: int = app.device_remove_registration(request_id=uuid4(), device_uuid=device_uuid)
    assert result == 1
    assert len(app.registered_devices) == 0

    result: int = app.device_remove_registration(request_id=uuid4(), device_uuid=device_uuid)
    assert result == 404


def test_generate_timestamped_raw_batch():
    raw_batch = RawBatch.generate_timestamped()
    pass


def test_generate_sampled_raw_batch():
    raw_batch = RawBatch.generate_sampled()
    pass

