import pytest

from uuid import UUID, uuid4
from queue import Queue

from ingress.processing.processor import IngressLayerProcessor
from ingress.kafka.production import BatchProducer
from crosscutting.entities.batch import Batch
from crosscutting.entities.rawbatch import RawBatch
from crosscutting.constants.kafka import KAFKA_BROKER


def test_batch_fields():
    raw_batch: RawBatch = RawBatch.generate_sampled()
    batch: Batch = Batch(raw_batch=raw_batch, location="test", production_line="test", machine="test")
    assert all([ key in batch.__dict__().keys() for key in ["location", "production_line", "machine"]])


def test_processor_add_metadata():
    raw_batch: RawBatch = RawBatch.generate_sampled()
    raw_batch_queue: Queue = Queue()
    kafka_producer: BatchProducer = BatchProducer(broker=KAFKA_BROKER)
    processor: IngressLayerProcessor = IngressLayerProcessor(
        IngressLayerProcessor.HardcodedMetadataProvider(device_uuids=[raw_batch.source_device_uuid]),
        raw_batch_queue=raw_batch_queue,
        batch_kafka_producer=kafka_producer
    )
    batch: Batch = processor.add_metata(raw_batch)
    assert type(batch) is Batch

    # raw_batch.source_device_uuid = uuid4()
    # with pytest.raises(ValueError):
    #     batch: Batch = processor.add_metata(raw_batch)


def test_batch_to_from_dict():
    raw_batch: RawBatch = RawBatch.generate_sampled()
    batch: Batch = Batch(raw_batch=raw_batch, location="test_loc", production_line="test_pl", machine="test_machine")
    dictionary: dict = batch.__dict__()
    assert all([key in dictionary.keys() for key in ["timestamp", "source_device_uuid", "values", "batch_uuid",
                                                     "location", "production_line", "machine"]])
    new_batch: Batch = Batch.from_dictionary(dictionary)
    assert batch.__dict__() == new_batch.__dict__()
