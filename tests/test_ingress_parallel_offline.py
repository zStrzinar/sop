import pytest, datetime, random, json
from uuid import UUID, uuid4
from typing import List

import redis

from ingress.parallel_app import ParallelIngressApp
from ingress.app import IngressApp, IngressAppSelector
from crosscutting.constants.redis import REDIS_HOST, REDIS_PORT
from crosscutting.entities.registration import Registration


def test_redis_connectivity():
    r = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)
    r.info()

    with pytest.raises(redis.exceptions.ConnectionError):
        redis.Redis(host=REDIS_HOST, port=REDIS_PORT - 1).info()

    pass


def test_device_registration_with_two_normal_apps():
    """ Create two instances of normal IngressApp, register a device to one of them, check if device is registered on
    the other one. It shouldn't be!"""

    app1: IngressApp = IngressApp()
    app2: IngressApp = IngressApp()
    device_uuid: UUID = uuid4()
    registration_timestamp: datetime.datetime = datetime.datetime.now(datetime.timezone.utc)
    check_timestamp: datetime.datetime = registration_timestamp + datetime.timedelta(seconds=1)

    def check_two_apps(app1: IngressApp, app2: IngressApp, device_uuid: UUID, time: datetime.datetime):
        return app1.is_device_registered(device_uuid, time), app2.is_device_registered(device_uuid, time)

    """ register to app1, app 2 souldn't have registration"""
    message, code = app1.device_registration(data=Registration(device_uuid=device_uuid).__dict__(), request_id=uuid4())
    assert code == 202

    assert check_two_apps(app1, app2, device_uuid, check_timestamp) == (1, 0)

    """ registration should still be in app1 (it wasn't overwritten by app2 initialization) """
    result: int = app1.is_device_registered(device_uuid=device_uuid, time=check_timestamp)
    assert result == 1

    """ register to app2 """
    message, code = app2.device_registration(data=Registration(device_uuid=device_uuid).__dict__(), request_id=uuid4())
    assert code == 202

    assert check_two_apps(app1, app2, device_uuid, check_timestamp) == (1, 1)

    """ unregister in app1, app2 should still have registration """
    code = app1.device_remove_registration(request_id=uuid4(), device_uuid=device_uuid)
    assert code == 1

    assert check_two_apps(app1, app2, device_uuid, check_timestamp) == (0, 1)


def test_init_parralel_app():
    namespace: str = 'abcdefgh'
    app: ParallelIngressApp = ParallelIngressApp(common_redis_namespace=namespace)
    assert app.REDIS_REGISTERED_DEVICES_KEY.find(namespace) > -1
    assert app.REDIS_REGISTERED_DEVICES_KEY.find(f".{namespace}.") > -1

    app2: ParallelIngressApp = ParallelIngressApp(common_redis_namespace=namespace)
    assert app.REDIS_REGISTERED_DEVICES_KEY == app2.REDIS_REGISTERED_DEVICES_KEY


def test_redis_list_handling():
    test_instance_id: str = str(uuid4())
    r: redis.Redis = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)
    l_int: List[int] = [random.randint(0, 100) for _ in range(100)]
    l_int_key: str = f"{test_instance_id}-l_int"
    r.set(name=l_int_key, value=json.dumps(l_int).encode('utf-8'))
    got = json.loads(r.get(l_int_key).decode('utf-8'))
    assert got == l_int

    r.delete(l_int_key)
    got = r.get(l_int_key)
    assert got is None
    pass


def test_registered_devices_redis_check():
    namespace: str = str(uuid4())
    app1: ParallelIngressApp = ParallelIngressApp(namespace)

    """ initialization: app1 initializes [] in redis """
    r: redis.Redis = redis.Redis(REDIS_HOST, REDIS_PORT)
    l: List[ParallelIngressApp.DeviceRegistration] = json.loads(
        r.get(app1.REDIS_REGISTERED_DEVICES_KEY).decode('utf-8'))
    assert l is not None
    assert l == []

    """ test setter """
    registration = ParallelIngressApp.DeviceRegistration(device_uuid=uuid4(),
                                                         registration_timestamp=datetime.datetime.utcnow(),
                                                         last_watchdog=datetime.datetime.utcnow(),
                                                         watchdog_interval=datetime.timedelta(minutes=60))
    app1.registered_devices = [registration]

    got: List[ParallelIngressApp.DeviceRegistration] = list(map(lambda o:
                                                                ParallelIngressApp.DeviceRegistration.from_dictionary(
                                                                    o),
                                                                json.loads(r.get(app1.REDIS_REGISTERED_DEVICES_KEY)
                                                                           .decode('utf-8'))))
    assert got == [registration]

    """ test getter """
    g = app1.registered_devices
    assert g == got

    """ test append """
    registration2 = ParallelIngressApp.DeviceRegistration(device_uuid=uuid4(),
                                                          registration_timestamp=datetime.datetime.utcnow(),
                                                          last_watchdog=datetime.datetime.utcnow(),
                                                          watchdog_interval=datetime.timedelta(minutes=60))
    app1._registered_devices_append(registration2)

    got2: List[ParallelIngressApp.DeviceRegistration] = list(map(lambda o:
                                                                ParallelIngressApp.DeviceRegistration.from_dictionary(
                                                                    o),
                                                                json.loads(r.get(app1.REDIS_REGISTERED_DEVICES_KEY)
                                                                           .decode('utf-8'))))
    assert got2 != got
    assert len(got2) == 2
    assert got2 == [registration, registration2]


def test_device_registration_with_two_parallel_apps():
    """ Create two instances of ParallelIngressApp, register a device to one of them, check if device is registered on
        the other """
    namespace: str = str(uuid4())
    app1: ParallelIngressApp = ParallelIngressApp(common_redis_namespace=namespace)
    app2: ParallelIngressApp = ParallelIngressApp(common_redis_namespace=namespace)
    device_uuid: UUID = uuid4()
    registration_timestamp: datetime.datetime = datetime.datetime.now(datetime.timezone.utc)
    check_timestamp: datetime.datetime = registration_timestamp + datetime.timedelta(seconds=1)

    def check_two_apps(app1: ParallelIngressApp, app2: ParallelIngressApp, device_uuid: UUID, time: datetime.datetime):
        return app1.is_device_registered(device_uuid, time), app2.is_device_registered(device_uuid, time)

    """ register to app1, app 2 sould also have registration"""
    message, code = app1.device_registration(data=Registration(device_uuid=device_uuid).__dict__(), request_id=uuid4())
    assert code == 202

    assert check_two_apps(app1, app2, device_uuid, check_timestamp) == (1, 1)

    """ third app shouldn't overwrite existing registrations made by first app """
    app3: ParallelIngressApp = ParallelIngressApp(common_redis_namespace=namespace)
    assert app1.is_device_registered(device_uuid, check_timestamp) == 1
    assert app2.is_device_registered(device_uuid, check_timestamp) == 1
    assert app3.is_device_registered(device_uuid, check_timestamp) == 1

    """ One un-registration should effect all three apps """
    result: int = app1.device_remove_registration(request_id=uuid4(), device_uuid=device_uuid)
    assert result == 1

    assert check_two_apps(app1, app2, device_uuid, check_timestamp) == (0, 0)
    assert check_two_apps(app1, app3, device_uuid, check_timestamp) == (0, 0)


def test_ingress_app_selector():
    namespace: str = str(uuid4())
    app1: ParallelIngressApp = ParallelIngressApp(namespace)
    app2: ParallelIngressApp = ParallelIngressApp(namespace)

    selector: IngressAppSelector = IngressAppSelector()
    selector.add_app(app1, 'app1_url')
    selector.add_app(app2, 'app2_url')

    assert selector.get_app('app1_url') == app1
    assert selector.get_app('app2_url') == app2

    with pytest.raises(KeyError):
        selector.get_app('app3_url')

    app3: ParallelIngressApp = ParallelIngressApp(namespace)
    with pytest.raises(KeyError):
        selector.add_app(app3, 'app1_url')

    pass
