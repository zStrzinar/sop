import json, random, time

from kafka.producer import KafkaProducer


def run_producer(topic: str, broker: str):
    producer = KafkaProducer(bootstrap_servers=broker, value_serializer=lambda x: json.dumps(x).encode('utf-8'))
    while True:
        data = {"data": random.random()}
        send_future = producer.send(topic, value=data)
        send_future.get(timeout=1)
        producer.flush()
        print(data)
        time.sleep(1)


def send_messages(broker: str, topic: str, messages: list, sleep: float = None):
    producer = KafkaProducer(bootstrap_servers=broker, value_serializer=lambda x: json.dumps(x).encode('utf-8'))
    for data in messages:
        assert type(data) is dict
        send_future = producer.send(topic, value=data)
        send_future.get(timeout=1)
        producer.flush()
        print(data)
        if sleep is not None:
            time.sleep(sleep)
        else:
            pass


if __name__ == '__main__':
    run_producer("test_topic", "192.168.1.111:9092")
