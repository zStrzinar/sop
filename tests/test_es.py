import pytest
import datetime
from uuid import uuid4
import random
import time

from analysis.db.es import ES
from crosscutting.constants.db import ES_HOST
from elasticsearch import Elasticsearch, ConnectionError


def test_connect():
    pass
    """ successfully connect """
    db = ES(host=ES_HOST, default_index="test-index")

    """ wrong address """
    with pytest.raises(ConnectionError):
        ES(host="localhost:12323", default_index="test-index")
    pass


def test_insert():
    db = ES(host=ES_HOST, default_index="test-index")
    data = {"test_key": "test_value", "test_int": 42}

    """ successfull call """
    result = db.insert(data)
    assert result == 1

    """ space in index """
    db.default_index = "index with spaces"
    with pytest.raises(Exception):
        db.insert(data)
    pass


def test_bulk_insert():
    db = ES(host=ES_HOST, default_index="test-index")
    data = [{"test_key": "test_value1", "test_int": 40},
            {"test_key": "test_value2", "test_int": 41},
            {"test_key": "test_value3", "test_int": 42},
            {"test_key": "test_value4", "test_int": 43},
            {"test_key": "test_value5", "test_int": 44}]

    """ successfull call """
    result: bool = db.bulk_insert(data)
    assert result is True
    pass


def get_random_datetime() -> datetime.datetime:
    return datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=random.uniform(0,3600*24))


def get_random_string(n=10):
    return ''.join([random.choice('abcdefghijklmnopqrtsuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') for _ in range(n)])


def test_get_by_match():
    db = ES(host=ES_HOST, default_index=f"test-index-{uuid4()}")
    data = [{"field1": get_random_string(), "field2": random.uniform(0, 100),
             "field3": get_random_datetime().isoformat('T')} for _ in range(10)]
    assert db.bulk_insert(data) is True
    time.sleep(1)

    d = random.choice(data)
    result = db.get_by_match(match_pairs=[(kv[0], kv[1]) for kv in d.items()])
    assert type(result) is list
    assert len(result) == 1
    assert type(result[0]) is dict
    assert result[0] == d

    result = db.get_by_match(match_pairs=[("field1", d["field1"])])
    assert type(result) is list
    assert len(result) == 1
    assert type(result[0]) is dict
    assert result[0] == d


def test_get_latest():
    db = ES(host=ES_HOST, default_index=f"test-index-{uuid4()}")
    data = [{"field1": get_random_string(), "field2": random.uniform(0, 100),
             "field3": get_random_datetime().isoformat('T')} for _ in range(10)]
    data[0]["field3"] = (datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(seconds=1)).isoformat('T')
    assert db.bulk_insert(data) is True
    time.sleep(1)
    result = db.get_latest(time_field="field3")
    assert result is not None
    assert type(result) is dict
    assert result == data[0]


def test_rebuild():
    from elasticsearch.exceptions import NotFoundError
    db = ES(host=ES_HOST, default_index=f"test-index-{uuid4()}")
    with pytest.raises(NotFoundError):
        db.client.indices.get(db.default_index)
    db.insert({"field1": get_random_string(), "field2": random.uniform(0, 100),
               "field3": get_random_datetime().isoformat('T')})
    time.sleep(1)
    db.client.indices.get(db.default_index)
    db.rebuild()
    with pytest.raises(NotFoundError):
        db.client.indices.get(db.default_index)
