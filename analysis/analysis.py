import queue
import threading
import logging

from analysis import ModelSyncRunner, AnalysisConsumerRunner, ArchiverRunner, ModelFacade, Model, EventDetectorRunner


class AnalysisApp:
    db_consumer: AnalysisConsumerRunner
    db_consumer_thread: threading.Thread
    db_segments_queue: queue.Queue

    segments_consumer: AnalysisConsumerRunner
    segments_consumer_thread: threading.Thread
    segments_queue: queue.Queue

    archive_runner: ArchiverRunner
    archive_thread: threading.Thread

    event_detector: EventDetectorRunner
    event_detector_thread: threading.Thread
    events_queue: queue.Queue

    model: ModelFacade

    model_sync: ModelSyncRunner
    model_sync_thread: threading.Thread

    event_archiver: ArchiverRunner
    event_archiver_thread: threading.Thread

    def __init__(self, archive_consumer: AnalysisConsumerRunner, segments_consumer: AnalysisConsumerRunner,
                 archive_runner: ArchiverRunner, event_detector: EventDetectorRunner, model: ModelFacade,
                 model_sync: ModelSyncRunner, event_archiver: ArchiverRunner):
        logger = logging.getLogger('sop-logger')
        self.db_consumer = archive_consumer
        logger.debug(f"Analysis layer initialized with archiving kafka consumer {self.db_consumer}")

        self.segments_consumer = segments_consumer
        logger.debug(f"Analysis layer initialized with event detection kafka consumer {self.segments_consumer}")

        self.archive_runner = archive_runner
        logger.debug(f"Analysis layer initialized with archiving runner {self.archive_runner}")

        self.event_detector = event_detector
        logger.debug(f"Analysis layer initialized with event detector {self.event_detector}")

        self.model = model
        logger.debug(f"Analysis layer initialized with model facade {self.model}")

        self.model_sync = model_sync
        logger.debug(f"Analysis layer initialized with model sync runner {self.model_sync}")

        self.event_archiver = event_archiver

        self.db_segments_queue = queue.Queue(maxsize=100)
        self.segments_queue = queue.Queue(maxsize=100)
        self.events_queue = queue.Queue(maxsize=100)
        pass

    def start(self):
        logger = logging.getLogger('sop-logger')
        logger.info("Starting Analysis layer")
        stop_event = threading.Event()
        self.db_consumer_thread = threading.Thread(target=self.db_consumer.run,
                                                   name="Analysis layer segments consumer for archiving",
                                                   args=(self.db_segments_queue, stop_event), daemon=True)
        self.segments_consumer_thread = threading.Thread(target=self.segments_consumer.run,
                                                         name="Analysis layer segments consumer for event detection",
                                                         args=(self.segments_queue, stop_event), daemon=True)
        self.archive_thread = threading.Thread(target=self.archive_runner.run,
                                               args=(self.db_segments_queue, stop_event, ), daemon=True,
                                               name="Analysis layer archive thread (write to DB)")
        self.event_detector_thread = threading.Thread(target=self.event_detector.run, daemon=True,
                                                      args=(self.segments_queue, self.events_queue, stop_event),
                                                      name="Analysis layere event detector")
        self.model_sync_thread = threading.Thread(target=self.model_sync.run, args=(stop_event, ), daemon=True,
                                                  name="Analysis layer model sync runner")
        self.event_archiver_thread = threading.Thread(target=self.event_archiver.run,
                                                      args=(self.events_queue, stop_event, ), daemon=True,
                                                      name="Events archiver thread (write to DB)")

        active_threads = [self.db_consumer_thread, self.segments_consumer_thread, self.archive_thread,
                           self.event_detector_thread, self.model_sync_thread, self.event_archiver_thread]

        for thread in active_threads:
            thread.start()

        logger.info("Threads started, starting infinite while loop for monitoring life of threads.")
        run = True
        while run:
            for thread in [self.db_consumer_thread, self.segments_consumer_thread, self.archive_thread,
                           self.event_detector_thread, self.model_sync_thread, self.event_archiver_thread]:
                thread: threading.Thread
                if not thread.is_alive():
                    logger.error(f"{thread.name} is dead. Ending execution of all other threads.")
                    run = False
        stop_event.set()
        for thread in active_threads:
            thread.join(timeout=1)
        pass
