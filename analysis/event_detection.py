import queue
import threading
import logging
from typing import List, Callable

from analysis.model import ModelFacade, Model, ModelResult
from crosscutting.entities.segment import Segment
from analysis.event import Event


class EventDetectorRunner:
    model: ModelFacade
    model_result_to_event: Callable

    def __init__(self, model_facade: ModelFacade, model_result_to_event: Callable):
        self.model = model_facade
        self.model_result_to_event = model_result_to_event
        pass

    def run(self, samples_queue: queue.Queue, events_queue: queue.Queue, stop_event: threading.Event):
        logger = logging.getLogger('sop-logger')
        logger.debug("EventDetectorRunner staring")
        event: dict = None
        while not stop_event.is_set() or event is not None:  # if an event was detected in samples - must not stop until
            # the event has been passed on
            if event is None:  # first process previous event!
                try:
                    segment: Segment = samples_queue.get(timeout=0.1)
                    logger.debug("Segment received")
                except queue.Empty:
                    continue
                assert type(segment) is Segment
                result: ModelResult = self.model.model.evaluate(segment)
                logger.debug("Segment evaluated")
                event: Event = self.model_result_to_event(result)
                logger.debug(f"Event generated: {event}")
            try:
                events_queue.put(event, timeout=0.1)
                logger.info(f"Event {event} put to events queue")
                event = None
            except queue.Full:
                logger.warning("Events queue if full")
                continue
