import threading
import time

class ModelSyncRunner:
    def __init__(self):
        pass

    def run(self, stop_event: threading.Event):
        while not stop_event.is_set():
            time.sleep(0.1)
