import pickle
import json
import datetime
from typing import List

from analysis import Model as BaseModel
from analysis.model import ModelResult as BaseModelResult
from crosscutting.entities.segment import Segment
from crosscutting.entities.values import SampledValues


class ModelResult(BaseModelResult):
    timestamp: datetime.datetime
    actual: float
    limit_under: float
    limit_over: float

    def __init__(self, actual: float, limit: float, timestamp: datetime.datetime):
        self.timestamp = timestamp
        self.actual = actual
        self.limit_under = limit
        self.limit_over = limit


class ExampleModel(BaseModel):
    class _State:
        n: int
        avg: float

        def __init__(self):
            self.n = 0
            self.avg = 0

        def __str__(self):
            return json.dumps({"n": self.n, "avg": self.avg})


    _internal_state: _State

    def __init__(self, learning_set: List[Segment]):
        self._internal_state = self._State()
        for segment in learning_set:
            self._evolve(segment)
        pass

    def _detect(self, sample: Segment) -> ModelResult:
        if type(sample.values) is SampledValues:
            values: List[float] = sample.values.values
        else:
            values: List[float] = list(map(lambda o: o.value , sample.values))
        sample_n = len(values)
        sample_avg: float = sum(values)/sample_n
        return ModelResult(sample_avg,  self._internal_state.avg, sample.timestamp)
        pass

    def _evolve(self, sample: Segment):
        if type(sample.values) is SampledValues:
            values: List[float] = sample.values.values
        else:
            values: List[float] = list(map(lambda o: o.value , sample.values))
        sample_n = len(values)
        sample_avg: float = sum(values)/sample_n
        self._internal_state.avg = (self._internal_state.avg * self._internal_state.n + sample_n * sample_avg)/(sample_avg + self._internal_state.n)
        self._internal_state.n = self._internal_state.n + sample_n
        pass

    @classmethod
    def from_blob(cls, blob: bytes):
        r = cls(learning_set=[])
        r._internal_state = pickle.loads(blob)
        return r

    # @classmethod
    # def from_dictionary(cls, dictionary: dict):
    #     pass

    def to_blob(self) -> bytes:
        return pickle.dumps(self._internal_state)

    def evaluate(self, sample: Segment) -> ModelResult:
        self._evolve(sample)
        return self._detect(sample)

    def __str__(self):
        return f"Example model with _internal_state: {self._internal_state}"
