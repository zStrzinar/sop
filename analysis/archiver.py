import threading
import queue
import logging

from crosscutting.entities.segment import Segment
from analysis.db.db import DB


class ArchiverRunner:
    db: DB
    _t: type

    def __init__(self, db: DB, _t: type):
        self.db = db
        self._t = _t
        pass

    def run(self, in_queue: queue.Queue, stop_event: threading.Event):
        logger = logging.getLogger('sop-logger')
        while not stop_event.is_set():
            try:
                element = in_queue.get(timeout=0.1)
                logger.debug(f"Received item {type(element)} for writing to DB")
            except queue.Empty:
                continue
            assert type(element) is self._t
            self.db.insert(element.__dict__())
            logger.debug("Item written to DB")
        pass
