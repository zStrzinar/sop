from typing import List, Tuple

from analysis.db.db import DB
from elasticsearch import Elasticsearch, ConnectionError


class ES(DB):
    client: Elasticsearch
    default_index: str

    def _check_index(self):
        if not self.default_index.find(" ") == -1:
            raise Exception("ES index must not include spaces!")

    def __init__(self, host: str, default_index: str):
        super().__init__(host)
        self.client = Elasticsearch(hosts=host)
        info: dict = self.client.info()
        if "tagline" not in info.keys():
            raise ConnectionError
        assert info["tagline"] == "You Know, for Search"
        self.default_index = default_index

    def insert(self, data: dict) -> bool:
        self._check_index()

        result: dict = self.client.index(index=self.default_index, body=data)
        return result['result'] == 'created'

    def bulk_insert(self, data: List[dict]) -> bool:
        self._check_index()

        body: List[dict] = []
        for o in data:
            body.append({"index": {"_index": self.default_index}})
            body.append(o)
        result: dict = self.client.bulk(body=body, index=self.default_index)
        return result['errors'] is False

    def get_latest(self, time_field: str, match_pairs: List[Tuple] = None):
        self._check_index()
        query_body: dict = {
            "sort": [
                {time_field: {"order": "desc"}}
            ],
            "size": 1
        }
        if match_pairs is not None:
            query_body["query"] = {
                "bool": {
                    "must": list(map(lambda o: {"match": {o[0]: o[1]}}, match_pairs))
                }
            }
        result: dict = self.client.search(body=query_body, index=self.default_index)
        hits = result['hits']['hits']
        if len(hits) == 1:
            return hits[0]['_source']
        elif len(hits) == 0:
            return None
        else:
            raise Exception

    def get_by_match(self, match_pairs: List[Tuple]) -> List[dict]:
        self._check_index()

        query_body: dict = {
            "query": {
                "bool": {
                    "must": list(map(lambda o: {"match": {o[0]: o[1]}}, match_pairs))
                }
            }
        }
        result = self.client.search(body=query_body, index=self.default_index)
        return list(map(lambda hit: hit['_source'], result['hits']['hits']))

    def get_by_key(self, key: str):
        self._check_index()

        result = self.client.get(index=self.default_index, id=key)
        pass

    def rebuild(self):
        from elasticsearch.exceptions import NotFoundError
        self._check_index()
        try:
            self.client.indices.get(self.default_index)
        except NotFoundError:
            return
        self.client.indices.delete(self.default_index)
        try:
            self.client.indices.get(self.default_index)
            raise Exception("Delete not successfull")
        except NotFoundError:
            return

    def __str__(self):
        return f"Elasticsearch database connection to {self.host}. Default index is {self.default_index}."