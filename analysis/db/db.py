from typing import List, Tuple


class DB:
    host: str
    client: object

    def __init__(self, host: str):
        self.host = host
        pass

    def insert(self, data: dict) -> bool:
        raise NotImplementedError

    def bulk_insert(self, data: List[dict]) -> bool:
        raise NotImplementedError

    def get_latest(self, time_field: str, match_pairs: List[Tuple] = None):
        raise NotImplementedError

    def get_by_match(self, match_pairs: List[Tuple]):
        raise NotImplementedError

    def get_by_key(self, key: str):
        raise NotImplementedError

    def rebuild(self):
        raise NotImplementedError

    def __str__(self):
        return f"Database connection to {self.host}"