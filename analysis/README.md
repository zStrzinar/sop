# Analysis layer
The purpose of any big-data system is to enable some data-driven actions. In our work, a common use case is event 
detection. In this prototype system, the analysis layer is where a machine learning algorithm is trained on gathered 
data, and actionable results are obtained.

The analysis layer can be divided into two parts:
1. Archiving - saving detected segments to a database to be used later as learning samples
2. Event detection - online running of the model

## Implementation
Implementation of the analysis layer is divided into two parts - archiving and event detection. Each of these is presented 
in the following chapters.

### Database communication
Database interaction is abstracted and generalized through the `analysis.db` module. There, a `DB` class can be found.
The class is meant as a base class for further implementations.

The methods expected are:
* `init`: the constructor, usually also defines the subset of database where our operations will occur (such as the 
table in a SQL DB) 
* `insert`: as the name suggests - insert one document. Documents are passed as dictionaries. If the connection is to a 
SQL DB the dictionary should be broken down, with some NoSQL DBs the dictionary can be directly inserted
* `bulk_insert`: same as insert, but take advantage of bulk operations on the DB
* `get_latest`: a timestamp field (column in SQL) should be provided and the latest entry will be retrieved. Optional: 
allow filtering before ordering by time.
* `get_by_match`: query by matching some properties/columns to some values. The name-value pairs to match should be 
passed as a list of tuples (name, value).
* `rebuild`: ensure a clean table - primarily necessary for testing

#### Elasticsearch
An example database connection class is implemented in `analysis.db.es.ES`. It enables connection to an 
[Elasticsearch](https://www.elastic.co/) database. It implements all the methods outlined above.

Interaction with an ES database is done through a REST API.

When creating an instance of an `ES` class the index the instance will operate on is defined. An index is similar to a 
table in a SQL DB. All operations are then done on this index.

#### How it is used

##### Archiving
An [archiver](https://bitbucket.org/zStrzinar/sop/src/master/analysis/archiver.py) thread is started.

The `Archiver` is agnostic to the database being used. It relies on the database passed being an implementation of the 
`DB` base class, and therefore supporting a predefined set of methods outlined above.

When running, the `Archiver` constantly checks the incoming queue. If a new item becomes available, it is inserted into 
the DB.

##### Event detection
In event detection, further explained below, the model is not directly accessed. A `ModelFacade` is 
[used](https://bitbucket.org/zStrzinar/sop/src/master/analysis/model.py). The facade screens the actual model and
handles accessing the database of our choosing for saving, updating, retrieving the model. In this way the user is not 
concerned with saving and retrieving the model to and from a DB, it is all seamlessly handled for him by the facade.

### Event detection
Event detection is the end goal of our prototype system. An `EventDetectorRunner` class is available. When `run` is
called it monitors the incoming samples queue (filled by a Kafka consumer on `SegmentsData` topic). Then a new segment
is received, it is passed on to a model facade for evaluation. After evaluation, an external (provided at 
`EventDetectorRunner` initialization), user-defined, `model_result_to_event(result: ModelResult) -> Event` function is 
called. The result of the function may be an `Event` object. The detected event is saved to a DB by passing it on to an 
`Archiver` instance (explained above).

Actual event detection is therefore performed by some user-provided model. The model is accessed through a 
`ModelFacade`.

#### ModelFacade
`ModelFacade` is implemented to handle creation, updating, retrieving of the model in connection with its database 
saved counterpart. A part of the facade is a database connection (an implementation od `DB` base class). The facade 
implements:
* `model` property: every access to the model can be regulated with the use of getters and setters
* `_update_from_db`: takes the model from DB, decodes it (in DB it is saved as a blob)
* `_update_in_db`: inserts a new version of the model into the DB
* `from_model`: a class method for creation of a facade when a model is already available. The model is saved to DB.
* `from_db`: a class method for creation of a facade when a model is available in DB. The model is retrieved from DB. 

#### Model
`Model` base class is available, it defines the methods required by any implementation of an event-detecting evolving
model:
* `_detect`: is the passed sample an event?
* `_evolve`: update internal state with the new passed sample
* `evaluate`: detect and evolve
* `from_blob`: a model can be retrieved from a DB, where it is saved as a Binary Large OBject.
* `to_blob`: a model must be saved to a DB as a Binary Large OBject

In example implementation of `Model` is 
[available](https://bitbucket.org/zStrzinar/sop/src/master/analysis/example/model.py).

## How to run
Analysis can be started using python3.8 as a module

``` python -m analysis ```

Several command line arguments are available, environment variable options are also available:

| argument | argument long | environment variable | description |
| ---      | ---           | ---                  |---          |
| -k       | --kafka       | KAFKA_BROKER         | Kafka broker host:port |
|          | --archive_consumer_group | ANALYSIS_ARCHIVE_CONSUMER_GROUP | Archiver (write to DB) consumer group |
|          | --events_consumer_group | ANALYSIS_EVENTS_CONSUMER_GROUP | Event detection consumer group |
| -e       | --use_elasticsearch | USE_ELASTICSEARCH  | Should use ElasticSearch as DB? |
| -d       | --db_host     | DB_HOST | host:port to use for accessing database |
| -a       | --es_archive_index | ES_ARCHIVE_INDEX | ES archive (segments) index |
| -m       | --es_model_index | ES_MODEL_INDEX | ES models index |
| -v       | --es_event_index | ES_EVENT_INDEX | ES detected events index |

Python package requirements file is `requirements.txt` in the root of this repository, and can be applied using 
``` pip install requirements.txt ```.

A `dockerfile` is available in the `docker/analysis` folder of this repository. The `docker/sop-base` image must first be
 built!
 
## How and what to modify
Any user should implement a real version od en event detection model, an implementation of the 
[base class](https://bitbucket.org/zStrzinar/sop/src/master/analysis/model.py). It must expose `evaluate`, `to_blob` and
`from_blob`.

## Future possibilities
Additional database connections should be supported. MYSQL has been kept in mind when creating the `DB` base class.

Improvements to the model-database-update mechanism are possible.
