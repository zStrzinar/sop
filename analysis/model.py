from analysis.db.db import DB
import threading
import pickle
import datetime


class ModelResult:
    def __init__(self):
        pass


class Model:
    def __init__(self):
        pass

    def evaluate(self, sample) -> ModelResult:
        raise NotImplementedError

    def _detect(self, sample) -> ModelResult:
        raise NotImplementedError

    def _evolve(self, sample):
        raise NotImplementedError

    @classmethod
    def from_blob(cls, blob: bytes):
        raise NotImplementedError

    def to_blob(self) -> bytes:
        raise NotImplementedError


class ModelDbEntry:
    model_identifier: str
    model: bytes
    timestamp: str
    model_key: str

    def __init__(self, model_identifier: str, model: bytes, timestamp: str, model_key: str):
        self.model_identifier = model_identifier
        self.model = model
        self.timestamp = timestamp
        self.model_key = model_key

    def to_dict(self):
        return {"model_identifier": self.model_identifier, self.model_key: self.model.decode('latin1'), "timestamp": self.timestamp}


class ModelFacade:
    db: DB
    __model: Model = None
    _model_type: type = Model
    db_key: str
    model_id: str
    db_timestamp: str

    @property
    def model(self):
        self._refresh()
        return self.__model

    @model.setter
    def model(self, value):
        self.__model = value
        # self._update_in_db(value)

    def __init__(self, db: DB, model_identifier: str, db_key: str, refresh_event: threading.Event):
        self.db = db
        self.model_id = model_identifier
        self.db_key = db_key
        self.refresh_event = refresh_event
        self.db_timestamp = "timestamp"
        pass

    def _update_from_db(self):
        db_entry: dict = self.db.get_latest(time_field=self.db_timestamp, match_pairs=[("model_identifier", self.model_id)])
        db_model_blob: bytes = db_entry[self.db_key].encode('latin1')
        self.__model = self._model_type.from_blob(db_model_blob)
        pass

    def _refresh(self):
        if self.__model is None or not self._is_model_valid():
            self._update_from_db()

    def _is_model_valid(self) -> bool:
        return not self.refresh_event.is_set()

    def _update_in_db(self):
        return self.db.insert(ModelDbEntry(model_identifier=self.model_id,
                                           timestamp=datetime.datetime.now(datetime.timezone.utc).isoformat('T'),
                                           model=self.model.to_blob(), model_key=self.db_key).to_dict())

    @classmethod
    def from_model(cls, db: DB, model_identifier: str, db_key: str, refresh_event: threading.Event, model: Model):
        facade = cls(db, model_identifier, db_key, refresh_event)
        facade.model = model
        facade._update_in_db()
        facade._model_type = type(model)
        return facade

    @classmethod
    def from_db(cls, db: DB, model_identifier: str, db_key: str, refresh_event: threading.Event, model_type: type):
        facade = cls(db, model_identifier, db_key, refresh_event)
        facade._model_type = model_type
        facade._update_from_db()
        return facade

    def __str__(self):
        return f"ModelFacade object connected to DB: {self.db}. Model ID = {self.model_id}. Model key in DB is " \
               f"{self.db_key}. Model timestamp is {self.db_timestamp}. Model = {self.model}."
