import argparse
import logging
import os
import threading

from typing import List, Callable
from uuid import UUID, uuid4

from analysis import AnalysisApp, AnalysisConsumerRunner, ArchiverRunner, EventDetectorRunner, ModelFacade, \
    ModelSyncRunner, Event
from analysis.db import DB, ES
from analysis.example.model import ModelResult, ExampleModel

from crosscutting.constants.kafka import KAFKA_BROKER, ANALYSIS_ARCHIVE_CONSUMER_GROUP, ANALYSIS_EVENTS_CONSUMER_GROUP
from crosscutting.constants.db import ES_HOST, ES_ARCHIVE_INDEX, ES_MODEL_INDEX, ES_EVENT_INDEX
from crosscutting.entities.segment import Segment

if __name__ == '__main__':
    # region logger
    logFormatter = logging.Formatter("%(asctime)s %(levelname)s @ %(threadName)s %(name)s: %(message)s")
    rootLogger = logging.getLogger("sop-logger")
    rootLogger.setLevel(logging.DEBUG)

    fileHandler = logging.FileHandler("log.log")
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)
    # endregion

    # region arguments
    parser = argparse.ArgumentParser(description="Analysis layer worker.")
    parser.add_argument("-k", "--kafka", help="Kafka broker host:port. KAFKA_BROKER environment variable.",
                        default=os.environ.get('KAFKA_BROKER', KAFKA_BROKER))
    parser.add_argument("--archive_consumer_group", help="Kafka consumer group for archiving. "
                                                         "ANALYSIS_ARCHIVE_CONSUMER_GROUP environment variable",
                        default=os.environ.get('ANALYSIS_ARCHIVE_CONSUMER_GROUP',
                                               ANALYSIS_ARCHIVE_CONSUMER_GROUP + str(uuid4())))
    parser.add_argument("--events_consumer_group", help="Kafka consumer group for archiving. "
                                                        "ANALYSIS_EVENTS_CONSUMER_GROUP environment variable",
                        default=os.environ.get('ANALYSIS_EVENTS_CONSUMER_GROUP',
                                               ANALYSIS_EVENTS_CONSUMER_GROUP + str(uuid4())))
    parser.add_argument("-e", "--use_elasticsearch", help="Use elasticsearch? USE_ELASTICSEARCH environment variable",
                        default=os.environ.get('USE_ELASTICSEARCH', "True"))
    parser.add_argument("-d", "--db_host", help="host:port for database. DB_HOST environment variable",
                        default=os.environ.get('DB_HOST', ES_HOST))
    parser.add_argument("-a", "--es_archive_index", help="Elasticsearch archiving index. ES_ARCHIVE_INDEX environment "
                                                         "variable",
                        default=os.environ.get('ES_ARCHIVE_INDEX', ES_ARCHIVE_INDEX))
    parser.add_argument("-m", "--es_model_index", help="Elasticsearch model index. ES_MODEL_INDEX environment "
                                                       "variable",
                        default=os.environ.get('ES_MODEL_INDEX', ES_MODEL_INDEX))
    parser.add_argument("-v", "--es_event_index", help="Elasticsearch model index. ES_EVENT_INDEX environment "
                                                       "variable",
                        default=os.environ.get('ES_EVENT_INDEX', ES_EVENT_INDEX))

    args = parser.parse_args()
    rootLogger.info(f"Arguments parsed: {args}")
    # endregion

    # region preparation for running
    archive_consumer = AnalysisConsumerRunner(kafka_broker=args.kafka, kafka_topic="SegmentsData",
                                              kafka_consumer_group=args.archive_consumer_group)
    rootLogger.debug(f"Archive kafka consumer initialized: {archive_consumer}")
    segments_consumer = AnalysisConsumerRunner(kafka_broker=args.kafka, kafka_topic="SegmentsData",
                                               kafka_consumer_group=args.events_consumer_group)
    rootLogger.debug(f"Event detection kafka consumer initialized: {segments_consumer}")

    if args.use_elasticsearch in [1, True, "True", "true", "1"]:
        db: ES = ES(host=args.db_host, default_index=args.es_archive_index)
        rootLogger.debug(f"ElasticSearch chosen as DB: {db}")
    else:
        raise NotImplementedError

    archive_runner = ArchiverRunner(db=db, _t=Segment)
    refresh_event = threading.Event()

    model_refresh_event = threading.Event()
    rootLogger.debug("Initializing model...")
    model = ModelFacade.from_model(db=ES(host=args.db_host, default_index=args.es_model_index),
                                   model_identifier=str(uuid4()), db_key="model", refresh_event=model_refresh_event,
                                   model=ExampleModel(learning_set=[]))
    rootLogger.debug("Model facade initialized, model written to DB")


    def model_result_to_event(model_result: ModelResult) -> Event:
        type: Event.EventType
        if model_result.actual > model_result.limit_over:
            type = Event.EventType.over
        elif model_result.actual < model_result.limit_under:
            type = Event.EventType.under
        else:
            return None
        return Event(timestamp=model_result.timestamp, type=type,
                     description=f"{type} event detected ({model_result.actual}@"
                                 f"{model_result.timestamp.isoformat('T')})")


    event_detector = EventDetectorRunner(model_facade=model, model_result_to_event=model_result_to_event)
    rootLogger.debug("Event detector initialized")

    event_archiver = ArchiverRunner(db=ES(host=args.db_host, default_index=args.es_event_index), _t=Event)
    rootLogger.debug("Events archiver initialized")

    model_sync = ModelSyncRunner()
    rootLogger.debug("Model sync runner initialized")

    app = AnalysisApp(archive_consumer=archive_consumer, segments_consumer=segments_consumer,
                      archive_runner=archive_runner, event_detector=event_detector, model=model,
                      model_sync=model_sync, event_archiver=event_archiver)
    rootLogger.debug("Analysis layer App initialized")
    # endregion

    rootLogger.info("Analysis layer all initialized. Starting App...")
    app.start()
    rootLogger.error("Analysis layer App stopped. Exiting.")
    pass
