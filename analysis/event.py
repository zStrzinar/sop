import datetime
from enum import Enum


class Event:
    class EventType(Enum):
        under = 0
        over = 1

        def __str__(self):
            return "under" if self == self.under else "over"

    timestamp: datetime.datetime
    type: EventType
    description: str

    def __init__(self, timestamp: datetime.datetime, type: EventType, description: str):
        self.timestamp = timestamp
        self.type = type
        self.description = description

    def __str__(self):
        return self.description

    def __dict__(self):
        return {
            "timestamp": self.timestamp.isoformat('T'),
            "type": str(self.EventType),
            "description": self.description
        }
