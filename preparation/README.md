# Preparation layer

Preparation layer acts as a middle layer, between 
[ingress](https://bitbucket.org/zStrzinar/sop/src/master/ingress/README.md) and 
[analysis](https://bitbucket.org/zStrzinar/sop/src/master/analysis/README.md) layers.

Ingress layer sends batches of data received from devices (with added metadata) to a kafka topic. Analysis is not
performed on batches but on meaningful segments. The purpose of the ingress layer is meaningful concatenation and 
segmentation of incoming data.

## Implementation

Preparation layer workers are Kafka consumers and producers. Each worker can be divided into three parts: a consumer
thread, a processor thread and a producer threads. The threads communicate using python queues. All three threads share a
common [threading.Event](https://docs.python.org/2/library/threading.html#event-objects) object capable of stopping 
their execution. All three threads are `daemon` threads, meaning that they run independently but are not awaited when 
the calling thread is exiting - they are killed if their parent is exiting.

### Segmentation

The purpose of this project is not the implementation of an actual machine learning algorithm, therefore segmentation is 
not truly implemented. A base class is provided but the user is required to provide his own segmentation algorithm.

For testing purposes a mock segmentation algorithm was implemented: see `TestSegmentor` class in [preparation layer
tests](https://bitbucket.org/zStrzinar/sop/src/master/tests/test_preparation.py). The `segment` method takes a 
[`Batch`](https://bitbucket.org/zStrzinar/sop/src/master/crosscutting/entities/batch.py) object and segments it into a 
list of [`Segment`](https://bitbucket.org/zStrzinar/sop/src/master/crosscutting/entities/segment.py) objects.

### Considerations
#### Segmentation buffer
Segment lengths are not pre-defined and are variable as we encounter different use cases. One segment may span less 
anywhere between a fraction of a batch, to several consecutive batches. The segmentor should, therefore, be able to buffer
batches of parts of batches. It would be beneficial if this buffer was designed in a way that would enable the workers'
restart.

#### Parallelism
We might be required to run several segmentation workers in parallel. How would this affect the design of the 
`Segmentor` class? Consecutive batches from the same device should be analysed together. How do we achieve this?

Two approaches are possible:
1. Batches from each device must always 'reach' the same segmentor.
2. Segmentation is completely independent of the segmentor instance that receives any batch. Several segmentor instances
may receive partial information, but the result will be a valid segmentation.

The first approach can be achieved by using Kafka. Kafka uses message keys to assign messages into topic partitions, and
each consumer is assigned a set of topic partitions. Therefore, as long as the set of consumers doesn't change and a
re-balancing event doesn't occur, messages from the same device can always be routed to the same segmentor if a device 
identifier is used as the Kafka message key. The problem with this approach is that re-balancing events may occur!
Every time a consumer joins or leaves a consumer group, a re-balancing happens, and continuity may be lost. This approach 
would enable us to scale the segmentation cluster, however, dynamic scaling as traffic increases would not be possible.

The second approach requires all workers to have access to a common pool of unassigned partial segments. 
[Redis](https://redis.io) would be a possible solution.

## How to run
The preparation layer can be run as a python module: `python -m preparation`. A docker image is also available.

Two command-line arguments are possible. Environment variable options are also available.

| argument | argument long | environment variable | description |
| ---      | ---           | ---                  |---          |
| -k       | --kafka       | KAFKA_BROKER         | Kafka broker host:port |
| -g       | --consumer_group | PREPARATION_CONSUMER_GROUP | Kafka consumer group for BatchData topic |

Python package requirements file is `requirements.txt` in the root of this repository, and can be applied using 
``` pip install requirements.txt ```.

## How and what to modify
As mentioned, segmentation should be implemented by the end-user. An instance of the new, custom, Segmentor class,
implementing a `segment(self, batch:Batch) -> List[Segment]` method should be passed to the preparation layer processor 
at initialization.

## Future possibilities
Further work can be done in testing different Segmentor architectures, including testing support for dynamic scaling.
