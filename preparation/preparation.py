import logging
import queue
import threading

from preparation.consumer import PreparationConsumer
from preparation.processor import PreparationProcessor
from preparation.producer import PreparationProducer


class PreparationApp:
    consumer: PreparationConsumer
    consumer_thread: threading.Thread
    batch_queue: queue.Queue

    producer: PreparationProducer
    producer_thread: threading.Thread
    segment_queue: queue.Queue

    processor: PreparationProcessor
    processor_thread: threading.Thread

    def __init__(self, kafka_consumer: PreparationConsumer, processor: PreparationProcessor,
                 kafka_producer: PreparationProducer):
        logger = logging.getLogger('sop-logger')

        self.consumer = kafka_consumer
        logger.debug(f"Preparation layer initialized with kafka consumer {kafka_consumer}")
        self.processor = processor
        logger.debug(f"Preparation layer initialized with processor {processor}")
        self.producer = kafka_producer
        logger.debug(f"Preparation layer initialized with kafka producer {kafka_producer}")

        self.batch_queue = queue.Queue(maxsize=100)
        self.segment_queue = queue.Queue(maxsize=100)
        logger.debug("Preparation layer queues (batch and segment queues) have max size 100")
        pass

    def start(self):
        logger = logging.getLogger('sop-logger')
        logger.info("Starting Preparation layer. Will run three threads.")
        stop_event = threading.Event()
        self.consumer_thread = threading.Thread(target=self.consumer.run, args=(self.batch_queue, stop_event),
                                                daemon=True, name="Kafka consumer in Preparation layer")
        self.processor_thread = threading.Thread(target=self.processor.run, args=(self.batch_queue, self.segment_queue,
                                                                                  stop_event), daemon=True,
                                                 name="Processor in Preparation layer")
        self.producer_thread = threading.Thread(target=self.producer.run, args=(self.segment_queue, stop_event),
                                                daemon=True, name="Kafka producer in Preparation layer")

        self.consumer_thread.start()
        logger.debug(f"Consumer thread started, daemon mode: {self.consumer_thread.isDaemon()}")
        self.processor_thread.start()
        logger.debug(f"Processor thread started, daemon mode: {self.processor_thread.isDaemon()}")
        self.producer_thread.start()
        logger.debug(f"Producer thread started, daemon mode: {self.producer_thread.isDaemon()}")

        logger.info("Threads started, starting infinite while loop for monitoring life of threads.")
        run: bool = True

        while run:
            for thread in [self.consumer_thread, self.processor_thread, self.producer_thread]:
                thread: threading.Thread
                if not thread.is_alive():
                    logger.error(f"{thread.name} thread is dead. Ending execution of all other threads!")
                    run = False
        stop_event.set()
        pass
