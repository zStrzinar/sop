import argparse
import logging
import os

from crosscutting.constants.kafka import KAFKA_BROKER, PREPARATION_CONSUMER_GROUP
from preparation.preparation import PreparationApp
from preparation.consumer import PreparationConsumer
from preparation.producer import PreparationProducer
from preparation.processor import PreparationProcessor, Segmentor
from crosscutting.entities.batch import Batch
from crosscutting.entities.segment import Segment
from typing import List


class TestSegmentor(Segmentor):
    def segment(self, batch: Batch) -> List[Segment]:
        batch_len: int = len(batch.values) if type(batch.values) is list else len(batch.values.values)
        segment_len: int = self.default_length
        segments: List[Segment] = []
        total_length: int = 0
        while total_length < batch_len:
            segments.append(Segment.generate_random(timestamp=batch.timestamp,
                                                    source_device_uuid=batch.source_device_uuid,
                                                    location=batch.location,
                                                    production_line=batch.production_line,
                                                    machine=batch.machine,
                                                    n=segment_len))
            total_length = total_length + segment_len
        return segments


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Preparation layer worker. Acts as a kafka consumer and producer. "
                                                 "Segments incoming batches into actionable segments.")
    parser.add_argument("-k", "--kafka", help="Kafka broker host:port. KAFKA_BROKER environment variable.",
                        default=os.environ.get('KAFKA_BROKER', KAFKA_BROKER))
    parser.add_argument('-g', '--consumer_group', help="Kafka consumer group",
                        default=os.environ.get('PREPARATION_CONSUMER_GROUP', PREPARATION_CONSUMER_GROUP))
    args = parser.parse_args()

    logFormatter = logging.Formatter("%(asctime)s %(levelname)s @ %(funcName)s: %(message)s")
    rootLogger = logging.getLogger("sop-logger")
    rootLogger.setLevel(logging.DEBUG)

    fileHandler = logging.FileHandler("log.log")
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)

    rootLogger.info("Initializing Preparation layer kafka consumer, procuder and procesor")
    consumer = PreparationConsumer(kafka_broker=args.kafka, kafka_topic="BatchData",
                                   kafka_consumer_group=args.consumer_group)
    producer = PreparationProducer(kafka_broker=args.kafka, kafka_topic="SegmentsData")
    processor = PreparationProcessor(TestSegmentor())
    rootLogger.debug("Preparation layer kafka consumer, procuder and procesor initialized")

    rootLogger.debug("Creating Preparation layer app")
    app = PreparationApp(kafka_consumer=consumer, kafka_producer=producer, processor=processor)
    rootLogger.info("Starting Preparation layer app")
    app.start()
    rootLogger.warning("Preparation layer app stopped.")

