import logging
import queue
import threading
import json

from kafka import KafkaProducer

from crosscutting.entities.segment import Segment


class PreparationProducer:
    kafka_broker: str
    kafka_segments_topic: str
    kafka_producer: KafkaProducer

    def __init__(self, kafka_broker: str, kafka_topic: str):
        self.kafka_broker = kafka_broker
        self.kafka_segments_topic = kafka_topic
        self.kafka_producer = KafkaProducer(bootstrap_servers=self.kafka_broker,
                                            value_serializer=lambda x: json.dumps(x).encode('utf-8'))

        logger = logging.getLogger('sop-logger')
        logger.info(f"Preparation layer kafka producer created, will connect to {self.kafka_broker} and send to topic "
                    f"{self.kafka_segments_topic}.")
        pass

    def run(self, segments_queue: queue.Queue, stop_event: threading.Event):
        logger = logging.getLogger('sop-logger')
        logger.info("Preparation layer producer started running.")
        while not stop_event.is_set():
            segment: Segment
            try:
                segment = segments_queue.get(timeout=0.1)  # don't block indefinitley - we need to check stop_event
            except queue.Empty:
                continue
            self._send(segment)
            logger.debug(f"Successfully sent one segment to kafka topic {self.kafka_segments_topic}:\n {segment}")
        logger.info("Peparation layer producer stop event received, exiting.")
        pass

    def _send(self, segment):
        segment_dict = segment.__dict__()
        assert type(segment_dict) is dict
        send_future = self.kafka_producer.send(self.kafka_segments_topic, value=segment_dict)
        send_future.get(timeout=1)
        self.kafka_producer.flush()
