import logging
import json
import queue
import threading
from uuid import UUID, uuid4

from kafka import TopicPartition
from kafka.consumer import KafkaConsumer

from crosscutting.entities.batch import Batch


class PreparationConsumer:
    kafka_broker: str
    kafka_batch_topic: str
    kafka_consumer_group: str
    consumer: KafkaConsumer
    convert_dict_to_batch: bool = True

    def __init__(self, kafka_broker: str, kafka_topic: str, kafka_consumer_group: str):
        logger = logging.getLogger('sop-logger')

        self.kafka_broker = kafka_broker
        self.kafka_batch_topic = kafka_topic
        self.kafka_consumer_group = kafka_consumer_group if kafka_consumer_group is not None else str(uuid4())

        self.consumer = KafkaConsumer(self.kafka_batch_topic, bootstrap_servers=[self.kafka_broker],
                                      auto_offset_reset='latest', enable_auto_commit=True,
                                      group_id=self.kafka_consumer_group,
                                      value_deserializer=lambda x: json.loads(x.decode('utf-8')))
        self.consumer.poll(timeout_ms=1, max_records=1)
        pass

    def __str__(self):
        return f"Preparation layer kafka consumer connected to {self.kafka_broker}, listening on topic " \
               f"{self.kafka_batch_topic} with consumer group id {self.kafka_consumer_group}"

    def run(self, batch_queue: queue.Queue, stop_event: threading.Event):
        logger = logging.getLogger('sop-logger')
        logger.debug(f"Starting kafka consumer {self.consumer}")
        while not stop_event.is_set():
            result = self.consumer.poll(timeout_ms=100, max_records=1)
            if result == {}:
                pass
            else:
                value = result[TopicPartition(self.kafka_batch_topic, 0)][0].value
                logger.debug(f"Received message: {value}")
                if self.convert_dict_to_batch:
                    try:
                        value = Batch.from_dictionary(value)
                        logger.debug("Dictionary converted to instance fo Batch class")
                    except (KeyError, AssertionError):
                        logger.error(f"Error converting {value} to instance of Batch class")
                        continue
                batch_queue.put(value)  # blocks if queue is full!
        pass
