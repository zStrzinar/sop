import logging
import queue
import threading
from typing import List

from crosscutting.entities.batch import Batch
from crosscutting.entities.segment import Segment


class Segmentor:
    default_length: int = 10

    def segment(self, batch: Batch) -> List[Segment]:
        raise NotImplementedError("This method must be overridden")


class PreparationProcessor:
    segmentor: Segmentor

    def __init__(self, segmentor: Segmentor):
        logger = logging.getLogger('sop-logger')
        self.segmentor = segmentor
        logger.debug(f"Preparation layer processor initialized with segmentor: {self.segmentor}")
        pass

    def run(self, in_queue: queue.Queue, out_queue: queue.Queue, stop_event: threading.Event):
        logger = logging.getLogger('sop-logger')

        while not stop_event.is_set():
            logger.debug(f"Getting an entry from batches queue {in_queue}")
            try: # must not block indefinitely because we would not see stop_event set and would not exit.
                data: Batch = in_queue.get(timeout=0.1)  # blocks until data is available
            except queue.Empty:
                continue
            logger.debug(f"Data received from batches queue: {data}")
            if type(data) is not Batch:
                logger.error(f"Value recived from eueue is not Batch instance but {type(data)}")
                continue
            #logger.debug("Batch successfully loaded into an in stance of Batch class")
            segments: List[Segment] = self.segmentor.segment(data)
            logger.debug(f"Batch successfully converted to {len(segments)} segments")
            logger.debug(f"Segments queue is {out_queue.qsize()} long before adding {len(segments)} segments")
            for s in segments:
                while not stop_event.is_set():
                    try:
                        out_queue.put(s, timeout=0.1)
                        break
                    except queue.Full:
                        pass
            logger.debug(f"Segments added, queue is now {out_queue.qsize()} long")

        logger.info("Preparation layer processor exited while loop. Ending processin.")
        pass
