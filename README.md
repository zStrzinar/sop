# IoT event detection system prototype #

This project implements a prototype architecture for an IoT event detection system. The event detection algorithm itself is not part of this project, only algorithm interfaces are defined. Implementation of the algorithm itself is left 
up to the end-user. 

## General overview of the project
The project is divided into four modules:
1. [Device simulator](https://bitbucket.org/zStrzinar/sop/src/master/device/)
2. [Ingress layer](https://bitbucket.org/zStrzinar/sop/src/master/ingress/)
3. [Preparation layer](https://bitbucket.org/zStrzinar/sop/src/master/preparation/)
4. [Analysis layer](https://bitbucket.org/zStrzinar/sop/src/master/analysis/)

All the modules share some common constants and entities found in 
[crosscutting](https://bitbucket.org/zStrzinar/sop/src/master/crosscutting/) folder.

Docker files are [available](https://bitbucket.org/zStrzinar/sop/src/master/docker/), and also a 
[docker-compose file](https://bitbucket.org/zStrzinar/sop/src/master/docker/docker-compose.yml).

## Introduction
To get a general understanding of the functions and inner-workings of each module, we recommend reading the individual readme
files for each module. The reading order should be:
[Ingress layer](https://bitbucket.org/zStrzinar/sop/src/master/ingress/),
[Device simulator](https://bitbucket.org/zStrzinar/sop/src/master/device/),
[Preparation layer](https://bitbucket.org/zStrzinar/sop/src/master/preparation/),
[Analysis layer](https://bitbucket.org/zStrzinar/sop/src/master/analysis/).

## What to modify
This repository is only a prototype and the end-user **should modify it with custom functionalities**. Each module 
README.md contains a section addressing this.

## How to run
Docker files are [available](https://bitbucket.org/zStrzinar/sop/src/master/docker/), and also a 
[docker-compose file](https://bitbucket.org/zStrzinar/sop/src/master/docker/docker-compose.yml).
Individual module docker files require the image `sop-base` to be build and available.

## Tests
Tests are available in the [tests]((https://bitbucket.org/zStrzinar/sop/src/master/tests/)) folder. 
[`requirements.txt`](https://bitbucket.org/zStrzinar/sop/src/master/requirements.txt) should be installed before 
running tests: `pip install requirements.txt`. Some tests also require some services running in the background. 
[`tests-docker-compose.yml`](https://bitbucket.org/zStrzinar/sop/src/master/docker/tests-docker-compose.yml) is provided
 for this purpose. Tests addressing the ingress REST API require the API to be running before running the tests. This 
 should be manually started `python -m ingress`.
 
## Contact
Žiga Stržinar: [mail](mailto:ziga.strzinar@gmail.com)
 
