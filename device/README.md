# Device simulator
Python module acting as a device. Generating random data and sending it to the ingress layer.
Currently, only REST API communication to Ingress layer is supported.

Since communication with ingress layer API is an essential part of device functionalities we advise the reader to first
read the [ingress layer README](https://bitbucket.org/zStrzinar/sop/src/master/ingress/README.md)

## Implementation
The device module is divided into two parts: the `Device` class and the `Simulator` class. The `Simulator` is the source of
measurement and uses the passed `Device` object for communication with Ingress layer.

The main purpose of this module is to enable the testing and development of other, more essential modules of the project.
The module is essentially a random generator.

### Communication options
Three communication modes for ingress layer communication are foreseen: 
[MQTT](https://www.hivemq.com/blog/how-to-get-started-with-mqtt/), [Kafka](https://kafka.apache.org/) and REST API, but 
only HTTP REST API is implemented. The API is described in 
[ingress layer README](https://bitbucket.org/zStrzinar/sop/src/master/ingress/README.md).

The channel type is chosen when initializing a `Device` object. The corresponding channel configuration should also be
passed to init function.

`Device` class public methods (eg. `try_reset_watchdog`, `try_register`, `try_unregister`,  `try_send_batch`) are
channel-type agnostic. They check the `Device.ingress_channel_type` property and call the appropriate private method
(eg. `_try_send_batch_API`).

### API communication
`Device` can:
1. Register itself
2. Unregister itself
3. Reset watchdog timer
4. Send batch data

For API documentation see [API README](https://bitbucket.org/zStrzinar/sop/src/master/ingress/README.md).
If a send is unsuccessful due to HTTP error code `401 UNAUTHORIZED` the device tries to re-register itself and retries
sending. If API is unavailable raw batches are saved in-memory in a buffer and resent once the API becomes available.

#### Parallel API considerations
Parallel ingress layer workers are implemented in a way that device is completely ignorant of the number of workers -
see API [docs](https://bitbucket.org/zStrzinar/sop/src/master/ingress/README.md).

### Simulator
`RandomDataSimulator` class is available. At initialization, it is configured with simulation properties (number of batches
per minute, size of batches, type of batches). The simulator then creates a `Device` object, registers it with the Ingress
layer and starts generating samples. The samples are then periodically sent to Ingress layer.

## How to run
The device can be started as a Python 3.8 module

`python -m device`

Several command line arguments are available, environment variable options are also available:

| argument | argument long | environment variable | description |
| ---      | ---           | ---                  |---          |
| -a       | --api         |                      | Use REST API for Ingress layer communication (default True)  |
| -i       | --bind        | INGRESS_API_HOST     | Ingress API host   |
| -p       | --port        | INGRESS_API_PORT     | Ingress API port   |

Python package requirements file is `requirements.txt` in the root of this repository, and can be applied using 
``` pip install requirements.txt ```.

A `dockerfile` is available in the `docker/device` folder of this repository. The `docker/sop-base` image must first be
 built!
 
 To run the device module, Ingress API must be up and running. See how to set it up 
 [here](https://bitbucket.org/zStrzinar/sop/src/master/ingress/README.md).

## How and what to modify
The `Device` class may be used with a different measurements source. The `Device` class will handle Ingress layer 
communication while the user-defined program should take care of acquiring and processing measurements. 

For critical implementations, it is recommended that the data source implements a buffer. The buffer will improve 
performance in unreliable network conditions and will enable local buffering of measurements if they can not be sent to 
the ingress layer. If possible store the buffer on disk not in-memory, to survive device resets etc.

## Future possibilities
Implementations of MQTT and Kafka producer in `Device` class are possible and would be beneficial in certain situations.
