import sys, argparse, os
from uuid import UUID, uuid4
from device.device import Device
from device.simulator import RandomDataSimulator
from crosscutting.constants.ingress import INGRESS_API_DEFAULT_HOST, INGRESS_API_DEFAULT_PORT

if __name__ == '__main__':
    use_ingress_api: bool = True
    parser = argparse.ArgumentParser("Device simulator")
    parser.add_argument("-a", "--api", help="Use REST api for Ingress layer communication", default=True)
    parser.add_argument("-i", "--host", help="Ingress layer REST API hostname. INGRESS_API_HOST environment variable.",
                        default=os.environ.get('INGRESS_API_HOST', INGRESS_API_DEFAULT_HOST))
    parser.add_argument("-p", "--port", help="Ingress layer REST API port. INGRESS_API_PORT environment variable.",
                        default=os.environ.get('INGRESS_API_PORT', INGRESS_API_DEFAULT_PORT))
    args = parser.parse_args()
    device_simulator: RandomDataSimulator = RandomDataSimulator(device=Device(uuid=uuid4()))
    device_simulator.device.ingress_channel_config.base_url = f"http://{args.host}:{args.port}"
    device_simulator.run()
