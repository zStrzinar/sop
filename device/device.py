from uuid import UUID, uuid4
from enum import Enum
from typing import Union, List
import json

import requests

from crosscutting.entities.rawbatch import RawBatch
from crosscutting.constants.ingress import INGRESS_API_DEFAULT_HOST as API_HOST, \
    INGRESS_API_DEFAULT_PORT as API_PORT, INGRESS_API_BATCH_ENDPOINT as API_BATCH_ENDPOINT, \
    INGRESS_API_REGISTRATION_ENDPOINT as API_REGISTRATION_ENDPOINT, \
    INGRESS_API_WATCHDOG_ENDPOINT as API_WATCHDOG_ENDPOINT


class DeviceIngressChannelType(Enum):
    API = 1
    Kafka = 2
    MQTT = 3


class Device:
    # region non-public classes
    class _APIChannelConfig:
        CHANNEL_TYPE: DeviceIngressChannelType = DeviceIngressChannelType.API
        base_url: str
        timeout: int

        def __init__(self, base_url: str = f"http://{API_HOST}:{API_PORT}", timeout: int = 10):
            self.base_url: str = base_url
            self.timeout = timeout

    class _KafkaChannelConfig:
        CHANNEL_TYPE: DeviceIngressChannelType = DeviceIngressChannelType.Kafka
        bootstrap_brokers: List[str]
        zookeeper: List[str]
        client_id: str
        consumer_group: str
        ingress_batch_topic: str

        def __init__(self):
            raise NotImplementedError

    class _MQTTChannelConfig:
        CHANNEL_TYPE: DeviceIngressChannelType = DeviceIngressChannelType.MQTT
        broker: str
        topic: str

        def __init__(self):
            raise NotImplementedError
    # endregion

    device_uuid: UUID
    ingress_channel_type: DeviceIngressChannelType
    ingress_channel_config: Union[_APIChannelConfig, _KafkaChannelConfig, _MQTTChannelConfig]

    # region constructiors
    def __init__(self, uuid: UUID,
                 ingress_channel_type: DeviceIngressChannelType = DeviceIngressChannelType.API,
                 ingress_channel_config: Union[_APIChannelConfig, _KafkaChannelConfig, _MQTTChannelConfig] = None):
        self.device_uuid = uuid

        type_config = [ingress_channel_type is not None, ingress_channel_config is not None]
        if type_config == [True, True]:
            assert ingress_channel_type == ingress_channel_config.CHANNEL_TYPE
            self.ingress_channel_type = ingress_channel_type
            self.ingress_channel_config = ingress_channel_config
        elif type_config == [True, False]:
            self.ingress_channel_type = ingress_channel_type
            if self.ingress_channel_type == DeviceIngressChannelType.API:
                self.ingress_channel_config = self._APIChannelConfig()
            elif self.ingress_channel_type == DeviceIngressChannelType.Kafka:
                self.ingress_channel_config = self._KafkaChannelConfig()
            elif self.ingress_channel_type == DeviceIngressChannelType.MQTT:
                self.ingress_channel_config = self._MQTTChannelConfig()
            else:
                raise NotImplementedError(f"Unknown ingress channel type {self.ingress_channel_type}")
        elif type_config == [False, True]:
            self.ingress_channel_config = ingress_channel_config
            self.ingress_channel_type = self.ingress_channel_config.CHANNEL_TYPE
        elif type_config == [False, False]:
            raise ValueError("Ingress channel type of config must be passed! None were.")

    @classmethod
    def get_new_api_device(cls, uuid_: UUID = None, register: bool = False):
        device = cls(uuid=uuid_ if uuid_ is not None else uuid4(), ingress_channel_type=DeviceIngressChannelType.API)
        if register:
            if not device.try_register():
                raise Exception("Device could not be registered")
        return device

    # endregion

    def try_send_batch(self, batch:RawBatch):
        if self.ingress_channel_type == DeviceIngressChannelType.API:
            return self._try_send_batch_API(batch)
        elif self.ingress_channel_type == DeviceIngressChannelType.Kafka:
            raise NotImplementedError
        elif self.ingress_channel_type == DeviceIngressChannelType.MQTT:
            raise NotImplementedError

    def _try_send_batch_API(self, batch: RawBatch, allow_retry: bool = True):
        response: requests.Response = requests.post(url=f"{self.ingress_channel_config.base_url}{API_BATCH_ENDPOINT}",
                                                    json=batch.__dict__(),
                                                    timeout=self.ingress_channel_config.timeout)
        if response.status_code == 201:
            pass
            return True
        elif response.status_code == 401 and allow_retry:
            self.try_reregister()
            return self._try_send_batch_API(batch, False)
        else:
            pass
            return False

    def try_reset_watchdog(self):
        if self.ingress_channel_type == DeviceIngressChannelType.API:
            return self._try_reset_watchdog_API()
        elif self.ingress_channel_type == DeviceIngressChannelType.Kafka:
            raise NotImplementedError
        elif self.ingress_channel_type == DeviceIngressChannelType.MQTT:
            raise NotImplementedError

    def _try_reset_watchdog_API(self, ttl: int = 1):
        from crosscutting.entities.watchdog import Watchdog
        if ttl < 0:
            return False

        response: requests.Response = requests.post(url=f"{self.ingress_channel_config.base_url}{API_WATCHDOG_ENDPOINT}",
                                                    json=Watchdog.from_device(self).__dict__(),
                                                    timeout=self.ingress_channel_config.timeout)
        if response.status_code == 200:
            pass
            return True
        elif response.status_code == 404:
            self.try_register()
            self._try_reset_watchdog_API(ttl=ttl-1)
        elif response.status_code == 410:
            self.try_reregister()
            self._try_reset_watchdog_API(ttl=ttl-1)
        else:
            return False

    def try_register(self):
        if self.ingress_channel_type == DeviceIngressChannelType.API:
            return self._try_register_API()
        elif self.ingress_channel_type == DeviceIngressChannelType.Kafka:
            raise NotImplementedError
        elif self.ingress_channel_type == DeviceIngressChannelType.MQTT:
            raise NotImplementedError
        pass

    def _try_register_API(self):
        from crosscutting.entities.registration import Registration
        response: requests.Response = requests.post(url=f"{self.ingress_channel_config.base_url}"
                                                        f"{API_REGISTRATION_ENDPOINT}",
                                                    json=Registration.from_device(self).__dict__(),
                                                    timeout=self.ingress_channel_config.timeout)

        if response.status_code == 202:
            return True
            pass
        elif response.status_code == 409:
            return False
            pass
        return False

    def try_unregister(self):
        if self.ingress_channel_type == DeviceIngressChannelType.API:
            return self._try_unregister_API()
        elif self.ingress_channel_type == DeviceIngressChannelType.Kafka:
            raise NotImplementedError
        elif self.ingress_channel_type == DeviceIngressChannelType.MQTT:
            raise NotImplementedError
        pass

    def _try_unregister_API(self):
        response: requests.Response = requests.delete(url=f"{self.ingress_channel_config.base_url}"
                                                          f"{API_REGISTRATION_ENDPOINT}/{self.device_uuid}",
                                                      timeout=self.ingress_channel_config.timeout)

        if response.status_code == 200:
            return True
            pass
        elif response.status_code == 404:
            return True
            pass
        return False

    def try_reregister(self):
        if self.try_unregister() is False:
            return False
        return self.try_register()
