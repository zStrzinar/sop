import datetime
import time
from typing import List
from requests.exceptions import ConnectionError
from uuid import UUID, uuid4

from device.device import Device
from crosscutting.entities.rawbatch import RawBatch, TimestampedValue, SampledValues


class RandomDataSimulator:
    device: Device
    batches_per_minte: float
    batch_size: int
    batch_type: type

    def __init__(self, device: Device,
                 batches_per_minute: float = 15.0,
                 batch_size:int = 3,
                 batch_type: type = SampledValues):
        self.device = device
        self.batches_per_minte = batches_per_minute
        self.batch_size = batch_size
        self.batch_type = batch_type
        pass

    def run(self):
        current_time = datetime.datetime.utcnow()
        next_run = current_time
        pause_between_runs = datetime.timedelta(minutes=1) / self.batches_per_minte
        reconnect_timeout: datetime.timedelta = datetime.timedelta(seconds=10)
        try:
            result = self.device.try_register()
            if result is True:
                print(f"[{self.device.device_uuid}] Registered with {self.device.ingress_channel_config.base_url}")
            else:
                print(f"[{self.device.device_uuid}] Could not register with {self.device.ingress_channel_config.base_url}. Ending.")
                return
        except ConnectionError:
            print(f"[{self.device.device_uuid}] Could not connect to {self.device.ingress_channel_config.base_url}. "
                  f"Retrying in {reconnect_timeout.total_seconds()} seconds.")
            try:
                time.sleep(reconnect_timeout.total_seconds())
            except KeyboardInterrupt:
                print(f"[{self.device.device_uuid}] Keyboard interrupt detected. Ending.")
                return
            return self.run()

        try:
            buffer: List[RawBatch] = []
            while True:
                batch: RawBatch = self._generate_batch()
                buffer.append(batch)
                time.sleep((next_run-current_time).total_seconds())
                current_time = next_run
                successfull_sends: List[RawBatch] = []
                try:
                    for batch in buffer:
                        result = self.device.try_send_batch(batch)
                        if result is True:
                            print(f"[{self.device.device_uuid}] Batch {batch.batch_uuid} successfully sent")
                            successfull_sends.append(batch)
                        else:
                            print(f"[{self.device.device_uuid}] Error {batch.batch_uuid}  while sending batch")
                    for success in successfull_sends:
                        buffer.remove(success)
                except ConnectionError:
                    print(f"[{self.device.device_uuid}] Connection error while connecting to "
                          f"{self.device.ingress_channel_config.base_url}.")

                next_run = current_time + pause_between_runs
                pass
        except KeyboardInterrupt:
            print(f"[{self.device.device_uuid}] KeyboardInterrupt (CTRL+C) detected. Unregistring")
            result = self.device.try_unregister()
            if result is True:
                print(f"[{self.device.device_uuid}] Unregistered")
            else:
                print(f"[{self.device.device_uuid}] Couldn't unregister")

    def _generate_batch(self) -> RawBatch:
        return RawBatch.generate_sampled(n=self.batch_size, source_device_uuid=self.device.device_uuid) \
            if self.batch_type is SampledValues else \
            RawBatch.generate_timestamped(n=self.batch_size, source_device_uuid=self.device.device_uuid)
