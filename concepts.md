# Common concepts
This section explains some common concepts and patterns found throughout this project.

## Apache Kafka
[Apache Kafka](https://kafka.apache.org/) is a distributed streaming platform. It is commonly used as a messaging 
system. It enables publishing and subscribing to data streams known as topics. One of its' important features is
the storage of messages for a predefined retention period.

Kafka is a distributed platform and can easily be run as a cluster to improve performance and ensure operation even with
some dead nodes.

### Message, topic, partition, offset
Every data published into a Kafka cluster is considered a `Message`. Every `Message` is published to exactly one 
`Topic`. Kafka cluster config can allow topics to be dynamically created by producers. 

A topic is divided into `Partitions`. The number of partitions is determined at topic creation and can not be changed 
later. Each `Message` is written to exactly one `Partition` at creation. The choice of `Partition` can be done by the 
producer or left up to the cluster. Every new message is appended to the end of a partition. The new message is provided
with a unique identifier - its' index inside the partition (see numbers in bellow image). The index is ever-increasing. Old
messages may be dropped from the partition after some time (a cluster configuration parameter), but old identifiers are 
never reused. These identifiers are called `Offsets`. A message in Kafka is uniquely identifiable through `Topic`-
`Partition`-`Offset` combination.

![Anatomy of a Kafka Topic](https://docs.confluent.io/current/_images/log_anatomy.png)

Partitions are useful because they enable a single Kafka topic to span several nodes (several servers) and overcome
disk limitations of a single server. Partitions can be replicated across several nodes to ensure uninterrupted 
operation and no loss of data even if some nodes are destroyed.

Partitions are further important for parallelization of message consumption as explained in the next section.

### Kafka consumers and consumer groups

`Partitions` mentioned in the previous section are important because every `Consumer` is only subscribed to a set
of `Partitions`, not the entire `Topic` (in general)!

An explanation: Every `Consumer` is part of a `Consumer group`. The Consumer group is subscribed to the entire Topic.
Every Consumer is assigned a set of Topic partitions. A Consumer then only receives messages published to the partitions
it was assigned to. If a consumer is disconnected the remaining consumers are reassigned its' partitions and consumption
of messages continues. Similarly - if a new consumer is added to the consumer group, it is assigned a fair portion of 
partitions. This enables parallelization of messages processing across multiple consumers. The consumer should, therefore, be
aware they may not receive all messages published to the topic, but only a subset!

#### Note regarding messages and partitions
It is possible to ensure a group of messages is written to the same partition. Besides a value, the message also 
contains a key. The key can be used to determine the partition where the message will be written. Messages with the same
key will be written to the same partition and processed by the same consumer (as long as a re-assignment doesn't occur).

### Use in this project
Kafka is used for communication between layers: Ingress->Preparation and Preparation->Analysis:
* Ingress layer implements a Kafka producer. It produces `Batch` messages into `BatchData` topic.
* Preparation layer implements a Kafka consumer. It consumes the `BatchData` topic.
* Preparation layer implements a Kafka producer sending `Segment` messages into `SegmentData` topic.
* Analysis layer includes a Kafka consumer listening on `SegmentData` topic.

## Module, App, Runner, Queue
Individual layers for this project are designed as python `modules`. They can be run:

`python -m module_name [args]`

### `__main__.py`
Each module includes a `__main__.py` file that is run when the module is started in the above manner. The `__main__.py`
script interprets command line arguments and environment variables (using `argparse` module). It also sets up a logger
(using `logging` module). It then configures and starts the modules' `App` class.

### `App`
The `App` class typically implements constructor (`__init__()`) and `start()` methods. Runners (explained later) usually
require some clients (DB connection, Kafka connections). Those are initialized in `__main__.py` or in `App.__init__()`.
`Start` method usually also creates the required `queues` for inter-runner communication.
Start runs the runners. Each runner is started in a separate thread (using `threading` module). The threads are
daemonized and are not awaited when exiting the application - they are killed.

### `Runner`
A runner is essentially an infinite loop (for details see below), typically consuming some data (from Kafka or a 
`Queue`), processing it, and passing it on (to Kafka or another `Queue`). The queue and Kafka clients must be passed
at initialization or (more frequently) at startup.

### `Queue`
Python [Queues](https://docs.python.org/3/library/queue.html) are our default communication channel between runners in 
the same app. Queues operate in FIFO (First-In-First-Out) mode. They enable thread-synchronization and provide temporary
resource locks to ensure atomic read and write operations. A request to retrieve data from an empty queue block by 
default. Similarly, a request to insert data to a full queue blocks until space is available.

## Runner design
As described above a runner is essentially an infinite loop. However, we still need the option to exit execution. For
this purpose a `stop_event` `threading.Event` object is passed to the runner. When the `Event` is set (default is not
set) by an external entity (such as the parent `App`) execution must exit.

The intuitive implementation  would be:
``` python
import threading
def run(self, some_parameres, stop_event: threading.Event):
    # some preparation steps
    while not stop_event.is_set()
        # some processing
```
Often in our applications `# some processing` would be retrieving data from a queue or listening to Kafka:
``` python
import threading
import queue

def run(self, in_queue: queue.Queue, stop_event: threading.Event):
    # some preparation steps
    while not stop_event.is_set()
        msg = in_queue.get()
        # some processing of received msg
``` 

At first glance this seems a valid program, however, `queue.get()` is a blocking operation! If `in_queue` is empty this
call blocks until a new item is enqueued. This will result in the `while` condition not evaluating and our thread being 
unable to stop. This is very possible when the application is shutting down and there may truly not be any un-processed
messages in the queue. 

The correct implementation is to set a `timeout` to `get()`, catch the resulting `queue.Empty()` Exception and continue
execution:

``` python
import threading
import queue

def run(self, some_arguments, in_queue: queue.Queue, stop_event: threading.Event):
    # some preparation steps
    while not stop_event.is_set():
        try:
            msg = in_queue.get(timeout=0.1)
        except queue.Empty:
            continue
        # some processing of received msg
```

The described pattern is repeated in several places in the project.
