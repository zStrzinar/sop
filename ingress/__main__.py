import queue, argparse, os, time, logging

from threading import Thread
from typing import List, Union
from uuid import uuid4
import kafka.errors

from ingress.app import IngressApp, IngressAPI, IngressAppSelector
from ingress.parallel_app import ParallelIngressApp
from crosscutting.constants.ingress import INGRESS_API_DEFAULT_HOST, INGRESS_API_DEFAULT_PORT
from crosscutting.constants.kafka import KAFKA_BROKER
from crosscutting.constants.redis import REDIS_HOST, REDIS_PORT
from ingress.processing.processor import IngressLayerProcessor
from ingress.kafka.production import BatchProducer



if __name__ == '__main__':
    """
    Run this file to run API as worker. Also run processor worker and forwarding worker.
    """
    parser = argparse.ArgumentParser(description="Ingress layer worker. \n Supports REST API, Kafka and MQTT for "
                                                 "device communication. \n Supports Kafka for backend communication.")
    parser.add_argument("-b", "--bind", help="Bind host. INGRESS_API_HOST environment variable.",
                        default=os.environ.get('INGRESS_API_HOST', INGRESS_API_DEFAULT_HOST))
    parser.add_argument("-p", "--port", help= "Bind port. INGRESS_API_PORT environment variable.",
                        default=os.environ.get('INGRESS_API_PORT', INGRESS_API_DEFAULT_PORT))
    parser.add_argument('-k', "--kafka", help="Kafka broker host:port. KAFKA_BROKER envirionment variable.",
                        default=os.environ.get('KAFKA_BROKER', KAFKA_BROKER))
    parser.add_argument('-c', "--cluster", help="Run in parallel mode. Requires Redis in-memory DB. INGRESS_PARALLEL "
                                                 "environment variable",
                        default=os.environ.get('INGRESS_PARALLEL', False))
    parser.add_argument('-n', '--namespace', help="Namespace for parallel running. INGRESS_REDIS_NAMESPACE enviornment "
                                                  "variable",
                        default=os.environ.get('INGRESS_REDIS_NAMESPACE', 'ab541bhpiru'))
    parser.add_argument("-r", "--redis", help="Redis host:port. REDIS_HOST environment variable.",
                        default=os.environ.get("REDIS_HOST", f"{REDIS_HOST}:{REDIS_PORT}"))
    parser.add_argument("-a", "--api", help="Use ingress REST API. INGRESS_USE_API environment variable",
                        default=os.environ.get('INGRESS_USE_API', True))

    args = parser.parse_args()

    logFormatter = logging.Formatter("%(asctime)s %(levelname)s @ %(funcName)s: %(message)s")
    rootLogger = logging.getLogger("sop-logger")
    rootLogger.setLevel(logging.DEBUG)

    fileHandler = logging.FileHandler("log.log")
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)

    use_api: bool = args.api in [True, "True", "true", 1, "1"]
    parallel: bool = args.cluster in [True, "True", "true", 1, "1"]

    rootLogger.debug(f"args.api = <{type(args.api)}> {args.api} --> use_api = {use_api}")
    rootLogger.debug(f"args.parallel = <{type(args.cluster)}> {args.cluster} --> parallel = {parallel}")

    rootLogger.debug(f"args={args}")

    raw_batches_queue: queue.Queue = queue.Queue(maxsize=50)
    threads: List[Thread] = []
    # region configure api
    if use_api:
        api: IngressAPI = IngressAPI()  # running API, getting requests
        app: Union[IngressApp, ParallelIngressApp]  # class with methods for processing requests
        if not parallel:
            app: IngressApp = IngressApp()
        else:
            app: ParallelIngressApp = ParallelIngressApp(common_redis_namespace=args.namespace,
                                                         redis_host=args.redis.split(':')[0],
                                                         redis_port=args.redis.split(':')[1])
        IngressAppSelector.add_app(app, f"single_app")  # selector will select correct app
        app.raw_batches_queue = raw_batches_queue
        api_thread = Thread(target=api.start, args=(args.bind, int(args.port)), daemon=True, name="API thread")
        threads.append(api_thread)
    # endregion

    # region configure processor
    while True:
        rootLogger.info(f"Tryping to connect to Kafka at {args.kafka}")
        try:
            batch_kafka_producer: BatchProducer = BatchProducer(args.kafka)
            rootLogger.info("Connection to kafka successfull")
            break
        except kafka.errors.NoBrokersAvailable:
            timeout = 10
            rootLogger.warning(f"Kafka connecting returned: NoBrokersAvailable. Retrying after {timeout} seconds")
            time.sleep(timeout)
            pass

    processor: IngressLayerProcessor = IngressLayerProcessor(
        metadata_provider=IngressLayerProcessor.HardcodedMetadataProvider(device_uuids=[uuid4()]),
        raw_batch_queue=raw_batches_queue,
        batch_kafka_producer=batch_kafka_producer)
    processor_thread: Thread = Thread(target=processor.run, daemon=True, name="IngressLayerProcessor")
    threads.append(processor_thread)
    # endregion

    for t in threads:
        rootLogger.debug(f"Starting {t.getName()} thread")
        t.start()

    while True:
        for t in threads:
            if not t.is_alive():
                rootLogger.fatal(f"Thread {t.getName()} is not alive! Exiting.")
                raise Exception(t)
        time.sleep(1)
