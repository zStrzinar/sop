import random
import logging

from uuid import UUID, uuid4
from typing import List, Union
import queue, time
from threading import Event
import asyncio

import kafka.errors

from crosscutting.entities.rawbatch import RawBatch
from crosscutting.entities.batch import Batch
from ingress.kafka.production import BatchProducer

class IngressLayerProcessor:
    # region internal classes
    class MetadataProvider:
        @classmethod
        def device_uuid_to_metadata(cls, device_uuid: UUID) -> dict:
            raise NotImplementedError

    class HardcodedMetadataProvider(MetadataProvider):
        mapping: dict = {}
        allowed_uuids: List[UUID] = []

        def __init__(self, device_uuids: List[UUID]):
            locations: List[str] = ["Ljubljana", "Maribor", "Celje", "Kranj", "Koper", "Lendava", "Novo Mesto", "Ptuj"]
            production_lines: List[str] = ["PL 1", "PL 2", "PL 3", "IML1", "IML 1", "IML 1" ]
            machines: List[str] = ["Labeling", "Quality control", "Manipulation robot", "Vision control"]
            for device_uuid in device_uuids:
                self.mapping[device_uuid] = {"location": random.choice(locations),
                                             "production_line": random.choice(production_lines),
                                             "machine": random.choice(machines)}
                self.allowed_uuids.append(device_uuid)

        def device_uuid_to_metadata(self, device_uuid: UUID):
            if device_uuid in self.mapping.keys():
                return self.mapping[device_uuid]
            else:
                # return None
                return self.mapping[self.allowed_uuids[0]]
    # endregion
    metadata_provider: Union[HardcodedMetadataProvider]
    raw_batch_queue: queue.Queue
    batch_producer: BatchProducer

    def __init__(self, metadata_provider: Union[HardcodedMetadataProvider], raw_batch_queue: queue.Queue,
                 batch_kafka_producer: BatchProducer):
        self.metadata_provider = metadata_provider
        self.raw_batch_queue = raw_batch_queue
        self.batch_producer = batch_kafka_producer
        self.logger = logging.getLogger('sop-logger')
        pass

    def process_raw_batch(self, raw_batch: RawBatch) -> Batch:
        return self.add_metata(raw_batch)
        pass

    def add_metata(self, raw_batch: RawBatch) -> Batch:
        metadata: dict = self.metadata_provider.device_uuid_to_metadata(raw_batch.source_device_uuid)
        if metadata is None:
            raise ValueError(f"no metadata found for given device_uuid {raw_batch.source_device_uuid}")
        return Batch(raw_batch=raw_batch, location=metadata["location"], production_line=metadata["production_line"],
                     machine=metadata["machine"])

    def run(self, cancelation_token: Event = Event()):
        while not cancelation_token.is_set():
            try:
                raw_batch: RawBatch = self.raw_batch_queue.get(timeout=0.1)
            except queue.Empty:
                continue
            self.logger.debug("Message received in queue")
            batch: Batch = self.process_raw_batch(raw_batch)
            while not cancelation_token.is_set():
                try:
                    self.batch_producer.send_batch(batch)
                    self.logger.debug("Message processed and sent to kafka as Batch")
                    break
                except:
                    self.logger.debug("Message sending to kafka (as Batch) failed. Retrying.")
                    time.sleep(1)
                    pass
