# Ingress layer
Python module acting as ingress layer:
* HTTP REST API ingress
* [Kafka](https://kafka.apache.org/) producer (output topic: BatchData)
* Supports parallel execution of multiple workers

The default listen port is 43521.

## Implementation
### Ingress API
#### Device registration
Before a device is allowed to send batch data for ingress, it must register. A `/registration` endpoint in implemented.
The request body must include the device unique identifier (`uuid`). Example request: 
```
POST ingress:43521/registration
{
    "device_uuid": "60e25766-e76e-4096-97dd-7686cc4cc749"
}
```

If the device uuid is already registered status code `409 CONFLICT` is returned. If registration is accepted 
`202 ACCEPTED`is returned.

The registration can also be performed with a `PUT` request on `/registration/<device_uuid>` endpoint. The return codes
are the same as above.

A device may also be unregistered using a `DELETE` od `/registration/<device_uuid>`. `200 OK`is returned for a
successful delete, `404 NOT FOUND` is returned if the device uuid was not registered at all.

#### Device watchdog
Once a device is registered a watchdog timer starts. The device must periodically reset the watchdog timer to provide  
proof of life, otherwise, it is disconnected and can no-longer post batches of measurements.

The `/watchdog` endpoint exposes a `POST` method with the body being the device uuid:
```
POST ingress:43521/watchdog
{
    "device_uuid": "60e25766-e76e-4096-97dd-7686cc4cc749"
}
```

Return codes are:

| return code | name | description |
| --- | --- | --- | 
| 200 | OK | Watchdog timer reset |
| 404 | NOT FOUND | Device is not registered |
| 406 | NOT ACCEPTABLE | Wrong request body format |
| 410 | GONE | Watchdog interval violated  |
 
#### Batch ingress
The main purpose of the ingress layer is to allow ingress of raw measurements from devices, add them the necessary 
metadata and forward to backend layers for further processing.

A `/batch` endpoint supporting `POST` is implemented. Two body formats are supported: one for timestamped raw 
measurements, and one for sampled raw measurements. Both request examples are shown below:

```
POST ingress:43521/registration
{
    "timestamp": "2020-07-28T15:19:00.000+02:00",
    "source_device_uuid": "60e25766-e76e-4096-97dd-7686cc4cc749",
    "values": {
        "sample_frequency_hz": 100,
        "values": [10, 11, 12, 13, 14, 15]
    },
    "batch_uuid": "35d167aa-bd26-4a23-8f18-657d34820ed1"
}
```

```
POST ingress:43521/registration
{
    "timestamp": "2020-07-28T15:19:00.000+02:00",
    "source_device_uuid": "60e25766-e76e-4096-97dd-7686cc4cc749",
    "values": [
        {
            "timestamp": "2020-07-28T15:19:00.000+02:00",
            "value": 12.5
        },
        {
            "timestamp": "2020-07-28T15:19:00.120+02:00",
            "value": 18.5
        },
        {
            "timestamp": "2020-07-28T15:19:00.431+02:00",
            "value": 11.9
        },
        {
            "timestamp": "2020-07-28T15:19:00.873+02:00",
            "value": 23.2
        }
    ],
    "batch_uuid": "35d167aa-bd26-4a23-8f18-657d34820ed1"
}
```

Return codes are:

| return code | name | description |
| --- | --- | --- | 
| 201 | CREATED | Batch accepted and passed on |
| 401 | UNAUTHORIZED | Device is not registered |
| 406 | NOT ACCEPTABLE | Wrong request body format |

Once a batch is received it is passed on for further processing. Communication to the next worker is done through a 
[python Queue](https://docs.python.org/3/library/queue.html).

### Processing
The purpose of the ingress layer processor is to add metadata to received raw batched. In this example metadata are 
`location`, `production line` and `machine` identifiers.

In the provided prototype example metadata is randomly chosen from a hardcoded list. However, the program is written so
that a new metadata provider may be created. It must implement the `ingress.processor.MetadataProvider` interface 
(extend the base class) and implement the `device_uuid_to_metadata(self, device_uuid: UUID) -> dict` method. The new 
provider should be passed to `IngressLayerProcessor` at initialization.

The processor received raw batch data in a queue, processes them (adds metadata) and sends them to Kafka using a Kafka 
producer passed at initialization.

### Kafka producer
Ingress layer uses a simple [Kafka](https://kafka.apache.org/) producer. It just provides a `send_batch` method.

### Parallel workers
It may be useful or even necessary to run multiple ingress API workers in parallel. This can be quite straightforward 
with some REST APIs, however, our API implements device registration and watchdog.

Imagine a scenario where we have two independent ingress layer API workers running: `worker A`, `worker B`.
`device1` comes online and sends register request. `worker A` accepts the registration and returns a `2XX`return code.
Later on, `device1` sends a batch of new measurements and due to load-balancing the request is sent to `worker B`. 
`worker B` doesn't have `device1` in its' registrations list and therefore rejects the batch.
Two solutions are possible:
1. Each device must only communicate with one worker.
2. Workers must synchronize their registration lists.

The first solution encounters problems if `device2` registers in a time of high traffic. The load balancer may start a 
new worker `worker C` to handle the high traffic, `device2` is registered with it. Later traffic decreases and we would 
now like to shut down some workers and lower costs. If `worker C` is shut down, `device2` can no longer communicate as 
it is unregistered. This solution is therefore impractical. 

The second solution allows requests to be given to any worker, but they all check the same registrations list. We 
chose this solution. The registrations list is saved in [Redis](https://redis.io/), a fast in-memory database. It 
operates as a key-value store. All workers access the same registrations list, check it, and update it. This in 
implemented in the `ParallelIngressApp` class that overrides some of the methods in the base `IngressApp` class.

In `ParallelIngressApp` every access to `self.registered_devices` is a query to redis.

Running in parallel mode is easiest when using docker-compose and its' scale functionality because docker-compose 
handles load balancing and routing.

## How to run
A worker can be started using python3.8 as a module

``` python -m ingress ```

Several command line arguments are available, environment variable options are also available:

| argument | argument long | environment variable | description |
| ---      | ---           | ---                  |---          |
| -b       | --bind        | INGRESS_API_HOST     | Bind host   |
| -p       | --port        | INGRESS_API_PORT     | Bind port   |
| -k       | --kafka       | KAFKA_BROKER         | Kafka broker host:port |
| -c       | --cluster     | INGRESS_PARALLEL     | Should run in parallel? |
| -n       | --namespace   | INGRESS_REDIS_NAMESPACE | Redis namespace for parallel running (key prefix) |
| -r       | --redis        | REDIS_HOST           | Redis host:port |

Python package requirements file is `requirements.txt` in root of this repository, and can be applied using 
``` pip install requirements.txt ```.

A `dockerfile` is available in the `docker/ingress` folder of this repository. The `docker/sop-base` image must first be
 built!

## How and what to modify
For serious use a custom `MetadataProvider` should be developed. It should extend the 
`ingress.processing.processor.IngressLayerProcessor.MetadataProvider` base class and implement a 
`device_uuid_to_metadata(cls, device_uuid: UUID) -> dict` class method. The returning dictionary should include 
`location`, `production_line` and `machine` keys.

## Future possibilities
It would be possible to extend the ingress layer to work with other forms of ingress besides a REST API: 
[MQTT](https://www.hivemq.com/blog/how-to-get-started-with-mqtt/) and Kafka consumer could be implemented.
