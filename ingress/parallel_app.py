import json, logging
from typing import List

import redis

from ingress.app import IngressApp
from crosscutting.constants.redis import REDIS_HOST, REDIS_PORT, REDIS_REGISTERED_DEVICES_KEY


class ParallelIngressApp(IngressApp):
    REDIS_REGISTERED_DEVICES_KEY: str
    r: redis.Redis

    @property
    def registered_devices(self) -> List[IngressApp.DeviceRegistration]:
        logger = logging.getLogger("sop-logger")
        logger.debug(f"Getting registered devices from redis (key = {self.REDIS_REGISTERED_DEVICES_KEY}")
        return list(map(lambda o: ParallelIngressApp.DeviceRegistration.from_dictionary(o),
                        json.loads(self.r.get(self.REDIS_REGISTERED_DEVICES_KEY)
                                   .decode('utf-8'))))

    @registered_devices.setter
    def registered_devices(self, value: List[IngressApp.DeviceRegistration]):
        logger = logging.getLogger("sop-logger")
        logger.debug(f"Setting registered devices to redis (key = {self.REDIS_REGISTERED_DEVICES_KEY}")
        assert type(value) is list
        self.r.set(name=self.REDIS_REGISTERED_DEVICES_KEY,
                   value=json.dumps(list(map(lambda o: o.__dict__(), value))).encode('utf-8'))

    def _registered_devices_append(self, value):
        logger = logging.getLogger("sop-logger")
        logger.debug(f"Appending registered devices with redis (key = {self.REDIS_REGISTERED_DEVICES_KEY}")
        current = self.registered_devices
        assert type(current) is list
        assert type(value) is ParallelIngressApp.DeviceRegistration
        current.append(value)
        assert type(current) is list
        self.registered_devices = current

    def _registered_devices_remove(self, value):
        logger = logging.getLogger("sop-logger")
        logger.debug(f"Removing registered device from redis (key = {self.REDIS_REGISTERED_DEVICES_KEY}")
        current = self.registered_devices
        assert type(current) is list
        assert type(value) is ParallelIngressApp.DeviceRegistration
        current.remove(value)
        assert type(current) is list
        self.registered_devices = current

    def __init__(self, common_redis_namespace: str, redis_host: str = REDIS_HOST, redis_port: int = REDIS_PORT):
        self.r = redis.Redis(host=redis_host, port=redis_port)
        self.REDIS_REGISTERED_DEVICES_KEY = REDIS_REGISTERED_DEVICES_KEY.format(common_redis_namespace)
        logger = logging.getLogger("sop-logger")

        if self.r.get(self.REDIS_REGISTERED_DEVICES_KEY) is None:
            logger.info(f"Initializing registered devices in redis (key = {self.REDIS_REGISTERED_DEVICES_KEY}")
            self.registered_devices = []
        else:
            logger.info(f"Registered devices are already in redis, not initializing. (key = "
                        f"{self.REDIS_REGISTERED_DEVICES_KEY}")

        self.raw_batches_queue = None
