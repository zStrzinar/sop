import datetime
from typing import Tuple, List
from uuid import UUID, uuid4
import queue
import logging

from flask import request
from flask_api import FlaskAPI, status
import dateutil.parser

from crosscutting.entities.rawbatch import RawBatch
from crosscutting.entities.registration import Registration
from crosscutting.entities.watchdog import Watchdog
from crosscutting.constants.ingress import INGRESS_API_DEFAULT_PORT, INGRESS_API_DEFAULT_HOST, INGRESS_WATCHDOG_INTERVAL


class IngressApp:
    class DeviceRegistration:
        device_uuid: UUID
        registration_timestamp: datetime.datetime
        last_watchdog: datetime.datetime
        watchdog_interval: datetime.timedelta

        def __init__(self, device_uuid: UUID, registration_timestamp: datetime.datetime,
                     last_watchdog: datetime.datetime, watchdog_interval: datetime.timedelta):
            self.device_uuid = device_uuid
            self.registration_timestamp = registration_timestamp
            self.last_watchdog = last_watchdog
            self.watchdog_interval = watchdog_interval

        def __dict__(self):
            return {
                "device_uuid": str(self.device_uuid),
                "registration_timestamp": self.registration_timestamp.isoformat('T'),
                "last_watchdog": self.last_watchdog.isoformat('T'),
                "watchdog_interval_seconds": self.watchdog_interval.total_seconds()
            }

        @classmethod
        def from_dictionary(cls, dictionary: dict):
            assert all([key in dictionary.keys() for key in ["device_uuid", "registration_timestamp", "last_watchdog",
                                                             "watchdog_interval_seconds"]])
            return cls(device_uuid=UUID(dictionary["device_uuid"]),
                       registration_timestamp=dateutil.parser.parse(dictionary["registration_timestamp"]),
                       last_watchdog=dateutil.parser.parse(dictionary["last_watchdog"]),
                       watchdog_interval=datetime.timedelta(seconds=dictionary["watchdog_interval_seconds"])
                       )

        def __eq__(self, other):
            return self.__dict__()==other.__dict__()


    __registered_devices: List[DeviceRegistration]

    @property
    def registered_devices(self) -> List[DeviceRegistration]:
        return self.__registered_devices

    @registered_devices.setter
    def registered_devices(self, value):
        self.__registered_devices = value

    def _registered_devices_append(self, value):
        self.registered_devices.append(value)

    def _registered_devices_remove(self, value):
        self.registered_devices.remove(value)

    raw_batches_queue: queue.Queue

    # def __init__(self, raw_batches_queue: queue.Queue):
    #     self.raw_batches_queue = raw_batches_queue

    def reinit(self):
        self.registered_devices = []

    def is_device_registered(self, device_uuid: UUID, time: datetime.datetime = datetime.datetime.now(datetime.timezone.utc)):
        """ Check if device is registered at passed time

        :param device_uuid: (UUID) uuid to check
        :param time: (datetime.datetime) time to check
        :return: int: 1 if device has valid registration, 0 device is not registered, -1 watchog missed or time passed
        is before registration timestamp
        :raises Exception: if more than one registrations with the same device_uuid are found
        """
        registrations: List[IngressApp.DeviceRegistration] = list(
            filter(lambda o: o.device_uuid == device_uuid, self.registered_devices))
        if len(registrations) == 0:
            return 0
        if len(registrations) > 1:
            raise Exception(f"device uuid {device_uuid} found {len(registrations)}-times in active registrations! Max "
                            f"one registration per device allowed!")
        registration: IngressApp.DeviceRegistration = registrations[0]
        if registration.last_watchdog + registration.watchdog_interval < time:
            return -1

        if registration.registration_timestamp > time:
            return -1

        return 1


    def batch_ingress(self, data: dict, request_id: UUID) -> Tuple[str, int]:
        """ Main processing function for /batch endpoint

        :param data: (dict) dictionary corresponding to RawBatch class - received data
        :param request_id: (UUiD) id used for logging
        :return: message, status_code: Status codes 201 Created, 401 Unauthorized, 406 Not acceptable or 500 Internal server error
        """
        try:
            raw_batch: RawBatch = RawBatch.from_dictionary(data)
            if self.is_device_registered(raw_batch.source_device_uuid, time=raw_batch.timestamp):
                self.raw_batches_queue.put(raw_batch)
            else:
                return "Device not registered!", status.HTTP_401_UNAUTHORIZED
        except Exception as e:
            print(f"[{request_id}] Error during conversion to RawBatch from dictionary: {e}")
            return "Deserialization error", status.HTTP_406_NOT_ACCEPTABLE
        return "Batch data accepted", status.HTTP_201_CREATED


    def device_registration(self, data: dict, request_id: UUID) -> Tuple[str, int]:
        """ Main processing function for /registration endpoint
        
        Registers the device (adds to list of registered devices). Fails if device is already registered.

        :param data: (dict) request body - a dictionary convertible to Registration class
        :param request_id: (UUID) request id
        :return: Message, status code (202 Accepted, 409 Conflict)
        """
        registration: Registration = Registration.from_dictionary(data)
        print(f"[{request_id}] successfully deserialized registration dictionary")
        print(f"[{request_id}] registration came from device {registration.device_uuid}")
        now: datetime.datetime = datetime.datetime.now(datetime.timezone.utc)

        if registration.device_uuid in list(map(lambda o: o.device_uuid, self.registered_devices)):
            print(f"[{request_id}] device {registration.device_uuid} already amongst registered devices. Rejecting "
                  f"registration.")
            return "Already registered", status.HTTP_409_CONFLICT

        start_length = len(self.registered_devices)
        self._registered_devices_append(IngressApp.DeviceRegistration(device_uuid=registration.device_uuid,
                                                                           registration_timestamp=now,
                                                                           last_watchdog=now,
                                                                           watchdog_interval=INGRESS_WATCHDOG_INTERVAL))
        assert len(self.registered_devices) == start_length + 1
        return "Registration successful", status.HTTP_202_ACCEPTED

    def device_remove_registration(self, request_id: UUID, device_uuid: UUID):
        """

        :param request_id:
        :param device_uuid:
        :return: 1: Registration removed, 404: No registration found for given device_uuid
        """

        registrations: List[IngressApp.DeviceRegistration] = list(filter(lambda o: o.device_uuid == device_uuid,
                                                                         self.registered_devices))
        if len(registrations) == 0:
            print(f"[{request_id}] No registrations found for device {device_uuid}")
            return 404

        for registration in registrations:
            self._registered_devices_remove(registration)

        return 1


    def register_device_watchdog(self, data: dict, time: datetime.datetime):
        """ Main processing function for /watchdog endpoint

        Checks if device is registered and watchog interval has not been violated.

        :param data:
        :param time:
        :return: 1 = OK, 0 = Not registered, -1 = Watchdog interval violated
        """
        try:
            watchdog: Watchdog = Watchdog.from_dictionary(data)
        except KeyError:
            return -2  # incorrect dictionary passed

        result: int = self.is_device_registered(device_uuid=watchdog.device_uuid, time=time)
        if result == -1:  # watchog interval violated
            return -1
        elif result == 0:  # not registered
            return 0
        elif result == 1:  # OK
            registrations: List[IngressApp.DeviceRegistration] = list(filter(lambda o: o.device_uuid == watchdog.device_uuid,
                                                                             self.registered_devices))
            assert len(registrations) == 1
            registration: IngressApp.DeviceRegistration = registrations[0]
            registration.last_watchdog = time
            return 1
        else:
            raise NotImplementedError(f"Unexpected result of App.is_device_registered(device_uuid={watchdog.device_uuid}, "
                                      f"time={time}): {result}")

    def start(self):
        pass

    def __init__(self):
        self.registered_devices = []
        self.raw_batches_queue = None


class IngressAPI:
    flask_api: FlaskAPI = FlaskAPI(__name__)

    @staticmethod
    @flask_api.route("/batch", methods=['POST'])
    def batch_ingress():
        """ /batch endpoint

        Supports POST method. request.data should be a dictionary (of class RawBatch).

        :return: message, status_code: Status codes 201 Created, 406 Not acceptable or 500 Internal server error
        """
        request_id: UUID = uuid4()
        logger = logging.getLogger("sop-logger")
        # ingress_app = IngressAppSelector.get_app(request.url_root)
        ingress_app = IngressAppSelector.get_app("single_app")
        logger.info(f"[{request_id}] {request.method}: {request.data}")
        if type(request.data) is not dict:
            logger.warning(f"{request_id} Request data type is not {type({})} but {type(request.data)}. Returning 406.")
            return f"Request data type is not {type({})} but {type(request.data)}", status.HTTP_406_NOT_ACCEPTABLE


        message, code = ingress_app.batch_ingress(dict(request.data), request_id=request_id)
        logger.info(f"[{request_id}] Request completed with code {code}")
        return message, code

    @staticmethod
    @flask_api.route('/registration/<device_uuid>', methods=['PUT', 'DELETE'])
    def device_registration_put_delete(device_uuid: str):
        """ /registration/<device_uuid> endpoint for direct PUT and DELETE

        :param device_uuid:
        :return: 200: Successfully deleted, 202: Registration created, 404: Registration not found for deletion,
        409: Registration already present, not adding)
        """
        request_id: UUID = uuid4()
        logger = logging.getLogger("sop-logger")
        device_uuid: UUID = UUID(device_uuid)
        # ingress_app = IngressAppSelector.get_app(request.url_root)
        ingress_app = IngressAppSelector.get_app("single_app")
        logger.info(f"[{request_id}] {request.method}: {request.data}")
        if request.method == 'PUT':
            message, code = ingress_app.device_registration(data={"device_uuid": str(device_uuid)},
                                                            request_id=request_id)
            logger.info(f"[{request_id}] Request completed with code {code}")
            return message, code
            pass
        elif request.method == 'DELETE':
            result: int = ingress_app.device_remove_registration(request_id=request_id, device_uuid=device_uuid)
            if result == 1:
                logger.info(f"[{request_id}] Registration for {device_uuid} removed. Returning 200")
                return "Removed", status.HTTP_200_OK
            elif result == 404:
                logger.info(f"[{request_id}] Registration for {device_uuid} not found. Returning 404")
                return "Not found", status.HTTP_404_NOT_FOUND
            else:
                logger.error(f"[{request_id}] Unexpected return code when removing device registration: {result}")
                raise NotImplementedError(f"Unexpected return code {result}")
        else:
            logger.error(f"[{request_id}] Unsupported method {request.method}. Returning 405.")
            return "Unsupported method", status.HTTP_405_METHOD_NOT_ALLOWED
            pass

    @staticmethod
    @flask_api.route('/registration', methods=['POST'])
    def device_registration():
        """ /registration endpoint

        Supports POST method. request data should be dictionary (of class Registration).
        Registers the device (adds to list of registered devices). Fails if device is already registered.

        :return: Message, status code (202 Accepted, 406 Not acceptable, 409 Conflict)
        """
        request_id: UUID = uuid4()
        logger = logging.getLogger("sop-logger")
        # ingress_app = IngressAppSelector.get_app(request.url_root)
        ingress_app = IngressAppSelector.get_app("single_app")
        logger.info(f"[{request_id}] {request.method}: {request.data}")
        if type(request.data) is not dict:
            logger.warning(f"{request_id} Request data type is not {type({})} but {type(request.data)}. Returning 406")
            return f"Request data type is not {type({})} but {type(request.data)}", status.HTTP_406_NOT_ACCEPTABLE

        message, code = ingress_app.device_registration(data=dict(request.data), request_id=request_id)
        logger.info(f"[{request_id}] Request completed with code {code}")
        return message, code

    @staticmethod
    @flask_api.route('/watchdog', methods=['GET', 'POST'])
    def device_watchdog():
        """ /watchdog endpoint - supports POST (GET not yet implemented)

        :return: 200 OK, 404 Not Found if device is not registered, 410 Gone if watchdog interval has been violated, 406 if type passed if wrong
        """

        request_id = uuid4()
        logger = logging.getLogger("sop-logger")
        # ingress_app = IngressAppSelector.get_app(request.url_root)
        ingress_app = IngressAppSelector.get_app("single_app")

        print(f"[{request_id}] {request.method}: {request.data}")
        if type(request.data) is not dict:
            logger.warning(f"{request_id} Request data type is not {type({})} but {type(request.data)}. Returning "
                           f"status 406")
            return f"Request data type is not {type({})} but {type(request.data)}", status.HTTP_406_NOT_ACCEPTABLE

        if request.method == 'GET':
            logger.error("GET to watchdog made! This is not implemented.")
            raise NotImplementedError("GET on /watchdog not yet implemented")
        elif request.method == 'POST':
            result: int = ingress_app.register_device_watchdog(dict(request.data), time=datetime.datetime.now(datetime.timezone.utc))
            if result == -1:  # watchdog interval violated
                logger.debug(f"[{request_id}] Watchdog interval has already been violated for device "
                      f"{request.data['device_uuid']}. Returning 410.")
                return "Wathchod interval violated", status.HTTP_410_GONE
            elif result == 0:  # device not registered
                logger.debug(f"[{request_id}] Device {request.data['device_uuid']} is not registed. Returning 404.")
                return "Device not registered", status.HTTP_404_NOT_FOUND
            elif result == 1:  # OK
                logger.debug(f"[{request_id}] Watchdog reset for device {request.data['device_uuid']}. Returning 200.")
                return "Wathdog reset", status.HTTP_200_OK
            elif result == 2:  # dictionary error
                logger.warning(f"[{request_id}] Request data format wrong: {request.data}. Returning 406.")
                return "Wrong format", status.HTTP_406_NOT_ACCEPTABLE
            else:
                logger.error(f"[{request_id}] Unexplected result of App._register_device_watchdog()={result}.")
                raise NotImplementedError(f"Unexplected result of App._register_device_watchdog()={result}.")
        else:
            logger.error(f"[{request_id}] Method not allowed! Returning 405.")
            return "Only GET and POST are allowed for /watchdog endpoint", status.HTTP_405_METHOD_NOT_ALLOWED

    def start(self, host: str = INGRESS_API_DEFAULT_HOST, port: int = INGRESS_API_DEFAULT_PORT):
        """ Start REST API

        By default binds INGRESS_API_DEFAULT_HOST:INGRESS_API_DEFAULT_PORT (localhost:42521)

        :param host: (str) hostname to bind
        :param port: (int) port number to bind
        :return: Doesn't return - API is started, the process is hijacked
        """
        #IngressAppSelector.add_app(IngressApp(), f"http://{host}:{port}")
        self.flask_api.run(host=host, port=port)

    def __init__(self):
        pass


class IngressAppSelector:
    apps: dict = {}

    @classmethod
    def add_app(cls, app: IngressApp, url_root: str):
        logger = logging.getLogger("sop-logger")
        if url_root in cls.apps.keys():
            logger.error(f"url_root {url_root} already registered!")
            raise KeyError(f"url_root {url_root} already registered!")
        cls.apps[url_root] = app
        logger.debug(f"app {app} added to IngressAppSelector with url {url_root}")

    @classmethod
    def get_app(cls, url_root: str):
        logger = logging.getLogger("sop-logger")
        if url_root not in cls.apps.keys():
            logger.error(f"no app found for url_root {url_root}. Available keys are: {','.join(cls.apps.keys())}")
            raise KeyError(f"no app found for url_root {url_root}")
        logger.debug(f"app successully found for url {url_root}")
        return cls.apps[url_root]

# if __name__ == '__main__':
#     """
#     Run this file to just run the API. API doesn't start as a worker, but as the main process.
#     """
#     import argparse
#
#     parser = argparse.ArgumentParser(description="Ingress layer worker. \n Supports REST API, Kafka and MQTT for "
#                                                  "device communication. \n Supports Kafka for backend communication.")
#     parser.add_argument("-b", "--bind", default=INGRESS_API_DEFAULT_HOST)
#     parser.add_argument("-p", "--port", default=INGRESS_API_DEFAULT_PORT)
#     args = parser.parse_args()
#     start_api: bool = True
#     raw_batches_queue: queue.Queue = queue.Queue(maxsize=50)
#     if start_api:
#         app: IngressApp = IngressApp()
#         IngressApp.raw_batches_queue = raw_batches_queue
#         app.start(host=args.bind, port=int(args.port))
