import json
from kafka.producer import KafkaProducer

from crosscutting.entities.batch import Batch


class BatchProducer:
    producer: KafkaProducer
    batch_topic: str = 'BatchData'
    broker: str

    def __init__(self, broker: str = 'localhost:9092'):
        self.broker = broker
        self.producer = KafkaProducer(bootstrap_servers=self.broker,
                                      value_serializer=lambda x: json.dumps(x).encode('utf-8'))

    def send_batch(self, batch: Batch) -> None:
        send_future = self.producer.send(self.batch_topic, value=batch.__dict__(),
                           key=str(batch.source_device_uuid).encode('utf-8'))
        send_future.get(timeout=1)
        self.producer.flush()
