from crosscutting.constants.network import CURRENT_IP
ES_HOST: str = f"{CURRENT_IP}:9200"
ES_ARCHIVE_INDEX: str = "archive-segments"
ES_MODEL_INDEX: str = 'models'
ES_EVENT_INDEX: str = 'events'
