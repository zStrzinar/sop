from crosscutting.constants.network import CURRENT_IP
KAFKA_BROKER: str = f"{CURRENT_IP}:9092"
PREPARATION_CONSUMER_GROUP: str = 'preparation-layer-cg1-v1'
ANALYSIS_ARCHIVE_CONSUMER_GROUP: str = 'analysis-layer-archive-cg1-v1'
ANALYSIS_EVENTS_CONSUMER_GROUP: str = 'analysis-layer-events-cg1-v1'
