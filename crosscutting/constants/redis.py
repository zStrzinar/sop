from crosscutting.constants.network import CURRENT_IP
REDIS_HOST: str = CURRENT_IP
REDIS_PORT: int = 6379
REDIS_REGISTERED_DEVICES_KEY: str = 'parallel_ingress_app.{0}.registred_devices'