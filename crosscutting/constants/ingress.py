import datetime
INGRESS_API_DEFAULT_HOST: str = 'localhost'
INGRESS_API_DEFAULT_PORT: int = 43521
INGRESS_API_BATCH_ENDPOINT: str = '/batch'
INGRESS_API_REGISTRATION_ENDPOINT: str = '/registration'
INGRESS_API_WATCHDOG_ENDPOINT: str = '/watchdog'
INGRESS_WATCHDOG_INTERVAL: datetime.timedelta = datetime.timedelta(seconds=3)
