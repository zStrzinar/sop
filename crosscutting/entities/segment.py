import datetime
from typing import Union, List
from uuid import UUID, uuid4

import dateutil.parser

from crosscutting.entities.batch import Batch
from crosscutting.entities.values import TimestampedValue, SampledValues


class Segment:
    timestamp: datetime.datetime
    source_device_uuid: UUID
    values: Union[List[TimestampedValue], SampledValues]
    segment_uuid: UUID
    location: str
    production_line: str
    machine: str

    def __init__(self, values: Union[List[TimestampedValue], SampledValues], timestamp: datetime.datetime,
                 source_device_uuid: UUID, segment_uuid: UUID, location: str, production_line: str, machine: str):
        self.values = values
        self.timestamp = timestamp
        self.source_device_uuid = source_device_uuid
        self.segment_uuid = segment_uuid
        self.location = location
        self.production_line = production_line
        self.machine = machine

    def __dict__(self):
        return {
            "timestamp": self.timestamp.isoformat('T'),
            "source_device_uuid": str(self.source_device_uuid),
            "values": list(map(lambda o: o.__dict__(), self.values)) if type(
                self.values) is list else self.values.__dict__(),
            "segment_uuid": str(self.segment_uuid),
            "location": self.location,
            "production_line": self.production_line,
            "machine": self.machine
        }

    @classmethod
    def from_dictionary(cls, dictionary: dict):
        if not all([key in dictionary.keys() for key in ["timestamp", "source_device_uuid", "values", "segment_uuid",
                                                         "location", "production_line", "machine"]]):
            raise KeyError(f"Missing keys in dictionary {dictionary} passed for deserialization to Segment.")

        if type(dictionary["values"]) is list:
            # type of dictionary["values"] is List[TimestampedValue]
            assert all([[ key in pair.keys() for key in ["timestamp", "value"]] for pair in dictionary["values"]])
            values: List[TimestampedValue] = [TimestampedValue(value["timestamp"], value["value"]) for value in dictionary["values"]]
        else:
            # type of dictionary["values"] is SampledValues
            assert type(dictionary["values"]) is dict
            assert all([ key in dictionary["values"].keys() for key in ["sample_frequency_hz", "values"]])
            values: SampledValues = SampledValues(sample_frequency_hz=dictionary["values"]["sample_frequency_hz"],
                                                  values=dictionary["values"]["values"])
        return cls(timestamp=dateutil.parser.parse(dictionary["timestamp"]),
                   source_device_uuid=UUID(str(dictionary["source_device_uuid"])),
                   values=values,
                   segment_uuid=UUID(str(dictionary["segment_uuid"])) if "segment_uuid" in dictionary.keys() else uuid4(),
                   location=dictionary["location"], production_line=dictionary["production_line"],
                   machine=dictionary["machine"])

    @classmethod
    def generate_random(cls,  timestamp: datetime.datetime = None, source_device_uuid: UUID = None,
                        segment_uuid: UUID = None, location: str = None, production_line: str = None,
                        machine: str = None, n: int = 20):
        from crosscutting.entities.rawbatch import RawBatch
        if timestamp is None:
            timestamp = datetime.datetime.now(datetime.timezone.utc)
        if source_device_uuid is None:
            source_device_uuid = uuid4()
        if segment_uuid is None:
            segment_uuid = uuid4()
        if location is None:
            location = str(uuid4())
        if production_line is None:
            production_line = str(uuid4())
        if machine is None:
            machine = str(uuid4())
        values = RawBatch.generate_timestamped(n=n, source_device_uuid=source_device_uuid,
                                               batch_uuid=segment_uuid).values
        return cls(values=values, timestamp=timestamp, source_device_uuid=source_device_uuid, segment_uuid=segment_uuid,
                   location=location, production_line=production_line, machine=machine)

