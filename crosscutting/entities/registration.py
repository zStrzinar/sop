from uuid import UUID, uuid4

from device.device import Device


class Registration:
    device_uuid: UUID

    def __init__(self, device_uuid: UUID):
        self.device_uuid = device_uuid
        pass

    @classmethod
    def from_device(cls, device: Device):
        return cls(device_uuid=device.device_uuid)

    @classmethod
    def from_dictionary(cls, dictionary: dict):
        assert [key in dictionary.keys() for key in ["device_uuid"]]

        return cls(device_uuid=UUID(dictionary["device_uuid"]))

    def __dict__(self):
        return {"device_uuid": str(self.device_uuid)}
