import datetime
import dateutil.parser
from uuid import UUID, uuid4
from typing import List, Union

#from device.device import Device
from crosscutting.entities.values import TimestampedValue, SampledValues


class RawBatch:
    timestamp: datetime.datetime
    source_device_uuid: UUID
    values: Union[List[TimestampedValue], SampledValues]
    batch_uuid: UUID

    def __init__(self,
                 timestamp: datetime.datetime,
                 source_device_uuid: UUID,
                 values: Union[List[TimestampedValue], SampledValues],
                 batch_uuid: UUID = uuid4()):
        self.timestamp = timestamp
        self.source_device_uuid = source_device_uuid
        self.values = values
        self.batch_uuid = batch_uuid
        pass

    # @classmethod
    # def generate_device_batch(cls,
    #                           device: Device,
    #                           timestamp: datetime.datetime,
    #                           values: Union[List[TimestampedValue], SampledValues]):
    #     assert type(device) is Device
    #     assert type(timestamp) is datetime
    #     assert type(values) in (list, SampledValues)
    #
    #     return cls(timestamp=timestamp,
    #                source_device_uuid=device.device_uuid,
    #                values=values)

    @classmethod
    def from_dictionary(cls, dictionary: dict):
        assert type(dictionary) is dict
        assert all([ key in dictionary.keys() for key in ["timestamp", "source_device_uuid", "values"]])
        if type(dictionary["values"]) is list:
            # type of dictionary["values"] is List[TimestampedValue]
            assert all([[ key in pair.keys() for key in ["timestamp", "value"]] for pair in dictionary["values"]])
            values: List[TimestampedValue] = [TimestampedValue(value["timestamp"], value["value"]) for value in dictionary["values"]]
        else:
            # type of dictionary["values"] is SampledValues
            assert type(dictionary["values"]) is dict
            assert all([ key in dictionary["values"].keys() for key in ["sample_frequency_hz", "values"]])
            values: SampledValues = SampledValues(sample_frequency_hz=dictionary["values"]["sample_frequency_hz"],
                                                  values=dictionary["values"]["values"])
        return cls(timestamp=dateutil.parser.parse(dictionary["timestamp"]),
                   source_device_uuid=UUID(str(dictionary["source_device_uuid"])),
                   values=values,
                   batch_uuid=UUID(str(dictionary["batch_uuid"])) if "batch_uuid" in dictionary.keys() else uuid4())

    # region helper random generators
    @classmethod
    def generate_timestamped(cls, n: int = 5, source_device_uuid: UUID = None, batch_uuid: UUID = None):
        import random
        values: List[float] = [random.uniform(-100, 100) for _ in range(n)]
        start_time: datetime.datetime = datetime.datetime.now(datetime.timezone.utc)
        if source_device_uuid is None:
            source_device_uuid = uuid4()
        if batch_uuid is None:
            batch_uuid = uuid4()
        timestamps: List[datetime.datetime] = list(map(lambda offset: start_time + datetime.timedelta(milliseconds=offset), sorted([random.uniform(1, 100) for _ in range(n)])))
        timestamped_values: List[TimestampedValue] = list(map(lambda i: TimestampedValue(timestamps[i], values[i]), range(n)))

        return cls(timestamps[-1], source_device_uuid=source_device_uuid, values=timestamped_values,
                   batch_uuid=batch_uuid)

    @classmethod
    def generate_sampled(cls, n: int = 5, source_device_uuid: UUID = None, batch_uuid: UUID = None):
        import random
        values: List[float] = [random.uniform(-100, 100) for _ in range(n)]
        end_time: datetime.datetime = datetime.datetime.now(datetime.timezone.utc)
        sample_frequency_hz: float = random.uniform(5, 500)
        if source_device_uuid is None:
            source_device_uuid = uuid4()
        if batch_uuid is None:
            batch_uuid = uuid4()
        return cls(timestamp=end_time,
                   source_device_uuid=source_device_uuid,
                   values=SampledValues(sample_frequency_hz=sample_frequency_hz,
                                        values=values),
                   batch_uuid=batch_uuid)
    # endregion

    def __dict__(self):
        return {
            "timestamp": self.timestamp.isoformat('T'),
            "source_device_uuid": str(self.source_device_uuid),
            "values": list(map(lambda o: o.__dict__(), self.values)) if type(self.values) is list else self.values.__dict__(),
            "batch_uuid": str(self.batch_uuid)
        }