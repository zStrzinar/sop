import datetime
import dateutil.parser
from typing import List


class TimestampedValue:
    timestamp: datetime.datetime
    value: float

    def __init__(self, timestamp: datetime.datetime, value: float):
        if type(timestamp) is datetime.datetime:
            pass
        elif type(timestamp) is str:
            timestamp = dateutil.parser.parse(timestamp)
        else:
            raise TypeError(timestamp)
        self.timestamp = timestamp
        self.value = value

    def __dict__(self):
        return {
            "timestamp": self.timestamp.isoformat('T'),
            "value": self.value
        }


class SampledValues:
    sample_frequency_hz: float
    values: List[float]

    def __init__(self, sample_frequency_hz: float, values: List[float]):
        self.sample_frequency_hz = sample_frequency_hz
        self.values = values

    def __dict__(self):
        return {
            "sample_frequency_hz": self.sample_frequency_hz,
            "values": self.values
        }
