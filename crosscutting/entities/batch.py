import datetime
from uuid import UUID, uuid4
from typing import Union, List

from crosscutting.entities.rawbatch import RawBatch
from crosscutting.entities.values import TimestampedValue, SampledValues


class Batch:
    timestamp: datetime.datetime
    source_device_uuid: UUID
    values: Union[List[TimestampedValue], SampledValues]
    batch_uuid: UUID
    location: str
    production_line: str
    machine: str

    def __init__(self, raw_batch: RawBatch, location: str, production_line: str, machine: str):
        self.timestamp = raw_batch.timestamp
        self.source_device_uuid = raw_batch.source_device_uuid
        self.values = raw_batch.values
        self.batch_uuid = raw_batch.batch_uuid
        self.location = location
        self.production_line = production_line
        self.machine = machine
        pass

    def __dict__(self):
        return {
            "timestamp": self.timestamp.isoformat('T'),
            "source_device_uuid": str(self.source_device_uuid),
            "values": list(map(lambda o: o.__dict__(), self.values)) if type(self.values) is list else self.values.__dict__(),
            "batch_uuid": str(self.batch_uuid),
            "location": self.location,
            "production_line": self.production_line,
            "machine": self.machine
        }

    @classmethod
    def from_dictionary(cls, dictionary: dict):
        if not all([key in dictionary.keys() for key in ["timestamp", "source_device_uuid", "values", "batch_uuid",
                                                         "location", "production_line", "machine"]]):
            raise KeyError(f"Missing keys in dictionary {dictionary} passed for deserialization to Batch.")
        raw_batch: RawBatch = RawBatch.from_dictionary(dictionary)
        return cls(raw_batch=raw_batch, location=dictionary["location"], production_line=dictionary["production_line"],
                   machine=dictionary["machine"])

    @classmethod
    def generate_random(cls,  timestamp: datetime.datetime = None, source_device_uuid: UUID = None,
                        batch_uuid: UUID = None, location: str = None, production_line: str = None,
                        machine: str = None, n: int = 100):
        from crosscutting.entities.rawbatch import RawBatch
        if timestamp is None:
            timestamp = datetime.datetime.now(datetime.timezone.utc)
        if source_device_uuid is None:
            source_device_uuid = uuid4()
        if batch_uuid is None:
            batch_uuid = uuid4()
        if location is None:
            location = str(uuid4())
        if production_line is None:
            production_line = str(uuid4())
        if machine is None:
            machine = str(uuid4())
        raw_batch = RawBatch.generate_timestamped(n=n, source_device_uuid=source_device_uuid, batch_uuid=batch_uuid)
        raw_batch.timestamp = timestamp
        return cls(raw_batch=raw_batch, location=location, production_line=production_line, machine=machine)
